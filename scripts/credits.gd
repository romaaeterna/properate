class_name Credits
extends Control


const animation_length = 90

@onready var center_credits = $CenterCredits
@onready var label_credits = center_credits.get_node("VBoxCredits/LabelCredits")
@onready var animation_credits = $AnimationCredits


func _on_ready():
    label_credits.text = label_credits.text.format({
        "game": tr("A Game by").to_upper(),
        "programming": tr("Programming & Design").to_upper(),
        "writing": tr("Writing").to_upper(),
        "snippets": tr("Code Snippets & Addons").to_upper(),
        "sprites": tr("Sprites").to_upper(),
        "textures": tr("Textures & Icons").to_upper(),
        "fonts": tr("Fonts").to_upper(),
        "music": tr("Music").to_upper(),
        "sounds": tr("Sounds").to_upper(),
        "licenses": tr("Licenses").to_upper(),
        "godot": tr("Made with Godot Engine 4"),
        "copyright": tr("© 2023/2024 Roma Aeterna")})
    var animation = animation_credits.get_animation("scroll")
    animation.length = animation_length
    animation.track_insert_key(0, animation_length, center_credits.size.y * -1)
    animation_credits.play("scroll")


func _on_button_back_pressed():
    scene_transition.change_scene("res://scenes/main.tscn")


func _on_label_credits_meta_hover_started(_meta):
    animation_credits.speed_scale = 0


func _on_label_credits_meta_hover_ended(_meta):
    animation_credits.speed_scale = 1


func _on_label_credits_meta_clicked(meta):
    OS.shell_open(str(meta))
