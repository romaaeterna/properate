class_name GameOnline
extends Node2D


const track_length = 1600

var checked_answer
var current_countdown
var current_event
var current_task
var current_team
var difficulty
var events_start
var events_meantime
var events_end
var game_stage
var tasks_easy
var tasks_medium
var tasks_hard
var team_number
var teams

var sound_event_bad = {"path": load("res://assets/sounds/event_bad.wav"), "db": -4}
var sound_event_good = {"path": load("res://assets/sounds/event_good.wav"), "db": -8}
var sound_game_start = {"path": load("res://assets/sounds/game_start.wav"), "db": -4}
var sound_victory = {"path": load("res://assets/sounds/victory.wav"), "db": -6}


@onready var texture_background = $TextureBackground
@onready var area_team1 = $AreaTeam1
@onready var area_team2 = $AreaTeam2
@onready var area_team3 = $AreaTeam3
@onready var area_team4 = $AreaTeam4
@onready var sprite_team1 = $AreaTeam1/SpriteTeam1
@onready var sprite_team2 = $AreaTeam2/SpriteTeam2
@onready var sprite_team3 = $AreaTeam3/SpriteTeam3
@onready var sprite_team4 = $AreaTeam4/SpriteTeam4
@onready var audio_stream = $AudioStreamSoundPlayer
@onready var timer_countdown = $TimerCountdown

@onready var timer_game_start = $GameStart/TimerGameStart
@onready var animation_game_start = $GameStart/AnimationGameStart
@onready var confetti = $GameEnd/Confetti
@onready var animation_game_end = $GameEnd/AnimationGameEnd
@onready var snow = $Snow

@onready var titlebar = $MarginTitlebar/HBoxTitlebar
@onready var icon_player = titlebar.get_node("IconPlayer")
@onready var label_player = titlebar.get_node("LabelPlayer")
@onready var button_menu = titlebar.get_node("ButtonMenu")
@onready var hbox_network = $MarginTitlebar/CenterNetwork/HBoxNetwork
@onready var label_message = hbox_network.get_node("LabelMessage")
@onready var label_time = hbox_network.get_node("LabelTime")

@onready var vbox_main = $TexturePanel/MarginMain/HBoxMain/VBoxMain
@onready var label_main = vbox_main.get_node("LabelMain")
@onready var hbox_answer = vbox_main.get_node("HBoxAnswer")
@onready var label_answer = hbox_answer.get_node("LabelAnswer")
@onready var line_answer = hbox_answer.get_node("LineAnswer")
@onready var label_solution = vbox_main.get_node("LabelSolution")
@onready var hbox_top = $TexturePanel/MarginMain/HBoxMain/VBoxMain/HBoxTop
@onready var label_topic = hbox_top.get_node("LabelTopic")
@onready var label_category = hbox_top.get_node("LabelCategory")
@onready var label_difficulty = hbox_top.get_node("LabelDifficulty")

@onready var hbox_feedback = $TexturePanel/MarginMain/HBoxMain/VBoxMain/HBoxFeedback
@onready var hbox_feedback1 = hbox_feedback.get_node("HBoxFeedback1")
@onready var rect_feedback1 = hbox_feedback1.get_node("RectFeedback1")
@onready var hbox_feedback2 = hbox_feedback.get_node("HBoxFeedback2")
@onready var rect_feedback2 = hbox_feedback2.get_node("RectFeedback2")
@onready var hbox_feedback3 = hbox_feedback.get_node("HBoxFeedback3")
@onready var rect_feedback3 = hbox_feedback3.get_node("RectFeedback3")
@onready var hbox_feedback4 = hbox_feedback.get_node("HBoxFeedback4")
@onready var rect_feedback4 = hbox_feedback4.get_node("RectFeedback4")

@onready var hbox_buttons = $TexturePanel/MarginMain/HBoxMain/VBoxMain/HBoxButtons
@onready var button_next = hbox_buttons.get_node("ButtonNext/LabelButtonNext")
@onready var button_easy = hbox_buttons.get_node("ButtonEasy/LabelButtonEasy")
@onready var button_medium = hbox_buttons.get_node("ButtonMedium/LabelButtonMedium")
@onready var button_hard = hbox_buttons.get_node("ButtonHard/LabelButtonHard")
@onready var button_right = hbox_buttons.get_node("ButtonRight/LabelButtonRight")
@onready var button_wrong = hbox_buttons.get_node("ButtonWrong/LabelButtonWrong")

@onready var message_dialog = $MessageDialog
@onready var vbox_dialog = $MessageDialog/MarginDialog/TextureDialog/MarginContent/VBoxContent
@onready var label_dialog = vbox_dialog.get_node("LabelDialog")
@onready var popup_settings = $PopupSettings


func _on_ready():
    connect_menu_button_popup()
    setup_multiplayer()
    set_background_texture()
    set_sprites_and_frames()
    set_teams()
    set_tasks()
    set_events()
    if global.music:
        music_player.stop()
    timer_game_start.start()


func _process(_delta):
    global.peer.poll()
    var state = global.peer.get_ready_state()
    if state == WebSocketPeer.STATE_OPEN:
        while global.peer.get_available_packet_count():
            packet_received(global.peer.get_packet())


func _on_menu_item_pressed(id):
    if id == 0:
        # New Game
        send_packet(global.clients, "client_left_game", [global.client_index])
        if global.host:
            send_packet(global.clients, "game_started", "false")
        scene_transition.change_scene("res://scenes/setup_online.tscn")
    if id == 1:
        # Settings
        global.last_scene = "game_online"
        var scene = load("res://scenes/settings.tscn").instantiate()
        popup_settings.add_child(scene)
        popup_settings.popup()
    if id == 2:
        # Main Menu
        global.peer.close()
        scene_transition.change_scene("res://scenes/main.tscn")
    if id == 3:
        # Quit Game
        global.peer.close()
        music_player.stop()
        scene_transition.fade_out()


func _on_button_next_pressed():
    timer_countdown.stop()
    if game_stage == "difficulty":
        stage_difficulty()
        send_packet(global.clients, "stage_difficulty", "")
        return
    if game_stage == "check_answer":
        stage_check_answer()
        send_packet(global.clients, "stage_check_answer", line_answer.text)
        return
    if game_stage == "victory":
        if global.host:
            send_packet(global.clients, "game_started", "false")
        scene_transition.change_scene("res://scenes/setup_online.tscn")


func _on_button_difficulty_pressed(button_number, button_text):
    difficulty = button_number
    stage_question(button_text)
    
    var data = str(difficulty) + "\t" + button_text
    send_packet(global.clients, "stage_question", data)


func _on_button_answer_pressed(right_answer):
    button_right.get_parent().hide()
    button_wrong.get_parent().hide()
    hbox_feedback.show()
    
    if global.host:
        get_answer_feedback(0, right_answer)
    else:
        show_player_feedback(global.client_index, right_answer)
    
    var data = str(global.client_index) + "\t" + str(right_answer)
    send_packet(global.clients, "stage_show_answer", data)


func _on_timer_countdown_timeout():
    current_countdown -= 1
    if current_countdown > 0:
        label_time.text = tr("%s s") % str(current_countdown).pad_zeros(2)
        timer_countdown.start()
    else:
        label_time.text = ""
        timer_countdown.stop()
        if game_stage == "difficulty":
            return stage_difficulty()
        if game_stage == "question" and global.client_index == current_team.number - 1:
            return _on_button_difficulty_pressed(3, "")
        if game_stage == "check_answer" and global.client_index == current_team.number - 1:
            return _on_button_next_pressed()
        if game_stage == "show_answer" and global.client_index != current_team.number - 1:
            return _on_button_answer_pressed(true)
        if game_stage == "movement":
            if current_event and label_topic.text != tr("Event"):
                return stage_event()
            else:
                return stage_movement()


func _on_timer_game_start_timeout():
    var animation = animation_game_start.get_animation("come_in")
    if 1 in global.clients:
        # enable animations for sprite of team 2
        animation.track_set_enabled(5, true)
    if 2 in global.clients:
        # enable animations for sprite of team 3
        animation.track_set_enabled(6, true)
    if 3 in global.clients:
        # enable animations for sprite of team 4
        animation.track_set_enabled(7, true)
    animation_game_start.play("come_in")
    play_sound(sound_game_start)


func _on_animation_game_start_finished(_animation_name):
    button_menu.disabled = false
    button_next.get_parent().disabled = false
    hbox_network.show()
    music_player.background_music.shuffle()
    music_player.play(music_player.background_music[0])


func _on_animation_game_end_finished(_animation_name):
    button_next.get_parent().disabled = false
    confetti.show()


func _on_animation_team_finished(_animation_name):
    current_team.sprite.play("idle")
    if game_stage == "victory":
        return await stage_victory()
    stage_difficulty()


func _on_button_dialog_pressed():
    message_dialog.hide()
    global.clients.erase(global.client_index)
    global.peer.close()
    scene_transition.change_scene("res://scenes/main.tscn")


func connect_menu_button_popup():
    var popup = button_menu.get_popup()
    popup.add_theme_constant_override("item_start_padding", 8)
    popup.add_theme_constant_override("item_end_padding", 8)
    popup.add_theme_constant_override("h_separation", 8)
    popup.add_theme_constant_override("v_separation", 8)
    popup.add_theme_color_override("font_color", Color(1, 1, 1, 1))
    popup.add_theme_color_override("font_separator_color", Color(1, 1, 1, 1))
    popup.add_theme_font_size_override("font_size", 30)
    popup.id_pressed.connect(_on_menu_item_pressed)


func setup_multiplayer():
    if global.host:
        var animation = animation_game_start.get_animation("come_in")
        animation.track_set_enabled(13, true)
    else:
        button_next.get_parent().hide()
        label_message.text = tr("Waiting for host …")


func set_background_texture():
    if global.background == "random":
        var backgrounds = ["desert", "forest", "grassland"]
        var index = randi() % len(backgrounds)
        global.background = backgrounds[index]
    if global.background == "desert":
        texture_background.texture = global.background_desert
    if global.background == "forest":
        texture_background.texture = global.background_forest
    if global.background == "grassland":
        texture_background.texture = global.background_grassland
    if Time.get_date_dict_from_system()["month"] == 12:
        snow.show()


func set_sprites_and_frames():
    global.team1.sprite = sprite_team1
    global.team2.sprite = sprite_team2
    global.team3.sprite = sprite_team3
    global.team4.sprite = sprite_team4
    sprite_team1.sprite_frames = global.team1.frames
    sprite_team2.sprite_frames = global.team2.frames
    sprite_team3.sprite_frames = global.team3.frames
    sprite_team4.sprite_frames = global.team4.frames
    
    for sprite in [sprite_team1, sprite_team2, sprite_team3, sprite_team4]:
        if sprite.sprite_frames:
            var frame_path = sprite.sprite_frames.resource_path
            if "adventure" in frame_path or "ninja" in frame_path or "robot" in frame_path:
                sprite.set_offset(Vector2(-13, 0))
            if "zombie" in frame_path:
                sprite.set_offset(Vector2(10, 0))


func set_teams():
    team_number = 1
    teams = [global.team1]
    sprite_team1.play("idle")
    if 1 in global.clients:
        team_number += 1
        teams.append(global.team2)
        area_team2.show()
        sprite_team2.play("idle")
    if 2 in global.clients:
        team_number += 1
        teams.append(global.team3)
        area_team3.show()
        sprite_team3.play("idle")
    if 3 in global.clients:
        team_number += 1
        teams.append(global.team4)
        area_team4.show()
        sprite_team4.play("idle")
    for team in teams:
        team.meters = 0
    
    label_main.text = tr("Be the first player to reach the finish line!")
    game_stage = "difficulty"


func set_tasks():
    tasks_easy = global.tasks_easy.duplicate(true)
    tasks_medium = global.tasks_medium.duplicate(true)
    tasks_hard = global.tasks_hard.duplicate(true)


func set_events():
    events_start = global.events_start.duplicate(true)
    events_meantime = global.events_meantime.duplicate(true)
    events_end = global.events_end.duplicate(true)
    current_event = null


func send_packet(receivers, topic, data):
    var packet = {"receivers": receivers, "topic": topic, "data": data}
    global.peer.send_text(JSON.stringify(packet, "  "))


func packet_received(packet):
    var json = JSON.new()
    var error = json.parse(packet.get_string_from_utf8())
    if error != OK:
        return
    
    var topic = json.data["topic"]
    var data = json.data["data"]
    
    if topic == "client_disconnected" or topic == "client_left_game":
        if topic == "client_disconnected":
            global.clients.erase(int(data[0]))
        if game_stage == "victory" or message_dialog.is_visible():
            return
        if int(data[0]) == 0:
            label_dialog.text = tr("The game was canceled by the host.")
        else:
            label_dialog.text = tr("Can't continue!\nPlayer %s left the game." % str(data[0] + 1))
        timer_countdown.stop()
        message_dialog.show()
        return
    
    if topic == "stage_difficulty":
        stage_difficulty()
        return
    
    if topic == "stage_question":
        var array = data.split("\t")
        difficulty = int(array[0])
        stage_question(array[1])
        return
    
    if topic == "get_question":
        var array = data.split("\t")
        label_category.text = array[0]
        label_main.text = array[1]
        label_solution.text = tr("Solution: %s") % array[2]
        return
    
    if topic == "stage_check_answer":
        line_answer.editable = false
        line_answer.text = data
        stage_check_answer()
        return
    
    if topic == "stage_show_answer":
        var array = data.split("\t")
        if global.host:
            return get_answer_feedback(int(array[0]), array[1].contains("true"))
        else:
            return show_player_feedback(int(array[0]), array[1].contains("true"))
    
    if topic == "answer_checked":
        return stage_show_answer(data.contains("true"))
    
    if topic == "get_event":
        var array = data.split("\t")
        current_event = global.event.new(0, array[0], float(array[1]))
        return


func stage_difficulty():
    checked_answer = []
    label_topic.add_theme_color_override("font_color", Color(0, 0, 0, 1))
    label_topic.text = tr("Difficulty")
    get_next_team()
    show_player_icon()
    var player_text = tr("Player %s: %s")
    label_player.text = player_text % [str(current_team.number), current_team.name]
    
    var nodes_to_hide = []
    var nodes_to_show = []
    if current_team.number == global.client_index + 1:
        label_message.text = ""
        label_main.text = tr("%s:\nChoose the difficulty of the next question!") % current_team.name
        nodes_to_hide = [label_category, label_difficulty, label_solution, hbox_answer, button_next,
                hbox_feedback, hbox_feedback1, hbox_feedback2, hbox_feedback3, hbox_feedback4]
        nodes_to_show = [label_main, button_easy, button_medium, button_hard]
    else:
        label_message.text = tr("Waiting for player %s … –") % str(current_team.number)
        label_main.text = tr("%s\nis choosing the difficulty of the next question.") % current_team.name
        nodes_to_hide = [label_category, label_difficulty, label_solution, hbox_answer, button_next,
                button_easy, button_medium, button_hard, hbox_feedback, hbox_feedback1,
                hbox_feedback2, hbox_feedback3, hbox_feedback4]
        nodes_to_show = [label_main]
    current_countdown = get_countdown_length()
    label_time.text = tr("%s s") % str(current_countdown).pad_zeros(2)
    
    game_stage = "question"
    hide_or_show_nodes(nodes_to_hide, nodes_to_show)
    timer_countdown.start()


func stage_question(difficulty_text):
    if global.host:
        get_question()
    
    label_topic.text = tr("Question")
    label_difficulty.text = difficulty_text
    
    var nodes_to_hide = []
    var nodes_to_show = []
    if current_team.number == global.client_index + 1:
        label_message.text = tr("Type in your answer! –")
        line_answer.editable = true
        line_answer.text = ""
        button_next.text = tr("Submit Answer")
        nodes_to_hide = [button_easy, button_medium, button_hard]
        nodes_to_show = [label_category, label_difficulty, hbox_answer, button_next]
    else:
        label_message.text = tr("Waiting for player %s … –") % str(current_team.number)
        nodes_to_show = [label_category, label_difficulty]
    current_countdown = get_countdown_length()
    label_time.text = tr("%s s") % str(current_countdown).pad_zeros(2)
    
    game_stage = "check_answer"
    hide_or_show_nodes(nodes_to_hide, nodes_to_show)
    timer_countdown.start()


func stage_check_answer():
    label_topic.text = tr("Check Answer")

    var nodes_to_hide = []
    var nodes_to_show = []
    if current_team.number == global.client_index + 1:
        if team_number > 2:
            label_message.text = tr("Waiting for other players … –")
        else:
            label_message.text = tr("Waiting for other player … –")
        line_answer.editable = false
        nodes_to_hide = [button_next]
    else:
        label_message.text = tr("Check the answer of player %s … –") % str(current_team.number)
        nodes_to_show = [hbox_answer, label_solution, button_right, button_wrong]
    current_countdown = get_countdown_length()
    label_time.text = tr("%s s") % str(current_countdown).pad_zeros(2)
    
    game_stage = "show_answer"
    hide_or_show_nodes(nodes_to_hide, nodes_to_show)
    timer_countdown.start()


func stage_show_answer(right_answer):
    current_countdown = get_countdown_length()
    label_time.text = tr("%s s") % str(current_countdown).pad_zeros(2)
    
    if right_answer:
        label_message.text = ""
        label_topic.add_theme_color_override("font_color", Color(0, 0.5, 0, 1))
        label_topic.text = tr("Right Answer")
        game_stage = "movement"
        if global.events and global.host:
            var random_number = randi_range(0, (1 / global.events) - 1)
            if random_number == 0:
                get_event()
    else:
        label_message.text = "Next Round –"
        label_topic.add_theme_color_override("font_color", Color(0.5, 0, 0, 1))
        label_topic.text = tr("Wrong Answer")
        game_stage = "difficulty"
    
    var nodes_to_hide = [button_right, button_wrong]
    var nodes_to_show = [label_solution, hbox_feedback]
    hide_or_show_nodes(nodes_to_hide, nodes_to_show)
    timer_countdown.start()


func stage_event():
    label_topic.add_theme_color_override("font_color", Color(0, 0, 0, 1))
    label_topic.text = tr("Event")
    label_main.text = current_event.text
    current_countdown = get_countdown_length()
    label_time.text = tr("%s s") % str(current_countdown).pad_zeros(2)
    
    var nodes_to_hide = [label_category, label_difficulty, hbox_answer, label_solution,
            hbox_feedback]
    hide_or_show_nodes(nodes_to_hide, [])
    timer_countdown.start()


func stage_movement():
    var animation_length = get_animation_length()
    game_stage = "difficulty"
    move_sprite(animation_length)


func stage_victory():
    label_topic.add_theme_color_override("font_color", Color(0, 0, 0, 1))
    label_topic.text = tr("Victory")
    label_main.add_theme_color_override("font_color", Color(0.5, 0, 0, 1))
    label_main.text = tr("The winner is:\n%s!") % current_team.name
    label_solution.add_theme_color_override("font_color", Color(0, 0, 0, 1))
    label_solution.text = tr("Congratulations!")
    button_next.text = tr("Rematch!")
    button_next.get_parent().disabled = true
    
    var nodes_to_hide = [label_category, label_difficulty, hbox_answer, hbox_feedback]
    hide_or_show_nodes(nodes_to_hide, [])
    
    animation_game_end.play("fade_in")
    music_player.stop()
    var sprites = [sprite_team1, sprite_team2, sprite_team3, sprite_team4]
    sprites.shuffle()
    for sprite in sprites:
        if sprite.get_parent().is_visible() and not sprite == current_team.sprite:
            var timer_length = randf_range(0.2, 0.5)
            await get_tree().create_timer(timer_length).timeout
            sprite.play("dead")
    play_sound(sound_victory)


func get_next_team():
    current_team = teams[0]
    teams.erase(current_team)
    if teams == []:
        teams = [global.team1]
        if 1 in global.clients:
            teams.append(global.team2)
        if 2 in global.clients:
            teams.append(global.team3)
        if 3 in global.clients:
            teams.append(global.team4)


func get_question():
    var index
    if difficulty == 1:
        index = randi() % len(tasks_easy)
        current_task = tasks_easy[index]
        tasks_easy.erase(current_task)
        if tasks_easy == []:
            tasks_easy = global.tasks_easy.duplicate(true)
    if difficulty == 2:
        index = randi() % len(tasks_medium)
        current_task = tasks_medium[index]
        tasks_medium.erase(current_task)
        if tasks_medium == []:
            tasks_medium = global.tasks_medium.duplicate(true)
    if difficulty == 3:
        index = randi() % len(tasks_hard)
        current_task = tasks_hard[index]
        tasks_hard.erase(current_task)
        if tasks_hard == []:
            tasks_hard = global.tasks_hard.duplicate(true)
 
    label_category.text = current_task.category
    label_main.text = current_task.question
    label_solution.text = tr("Solution: %s") % current_task.answer
    
    var data = current_task.category + "\t" + current_task.question + "\t" + current_task.answer
    send_packet(global.clients, "get_question", data)


func get_answer_feedback(client_index, right_answer):
    show_player_feedback(client_index, right_answer)
    checked_answer.append(right_answer)
    if len(checked_answer) == team_number - 1:
        timer_countdown.stop()
        var boolean
        if checked_answer.count(true) > checked_answer.count(false):
            stage_show_answer(true)
            boolean = "true"
        else:
            stage_show_answer(false)
            boolean = "false"
        send_packet(global.clients, "answer_checked", boolean)


func get_event():
    var random_number
    if current_team.meters == 0:
        random_number = randi() % len(events_start)
        current_event = events_start[random_number]
        events_start.erase(current_event)
        if events_start == []:
            events_start = global.events_start.duplicate(true)
    elif current_team.meters > 1200:
        random_number = randi() % len(events_end)
        current_event = events_end[random_number]
        events_end.erase(current_event)
        if events_end == []:
            events_end = global.events_end.duplicate(true)
    else:
        random_number = randi() % len(events_meantime)
        current_event = events_meantime[random_number]
        events_meantime.erase(current_event)
        if events_meantime == []:
            events_meantime = global.events_meantime.duplicate(true)
    
    var data = current_event.text + "\t" + str(current_event.modificator)
    send_packet(global.clients, "get_event", data)   


func get_countdown_length():
    if game_stage == "difficulty":
        if global.online_timer == 1: return 5
        if global.online_timer == 2: return 10
        if global.online_timer == 3: return 15
    if game_stage in ["question", "check_answer"]:
        if global.online_timer == 1: return 15
        if global.online_timer == 2: return 30
        if global.online_timer == 3: return 45
    if game_stage in ["show_answer", "movement"]:
        if global.online_timer == 1: return 3
        if global.online_timer == 2: return 5
        if global.online_timer == 3: return 10


func get_animation_length():
    var animation_length
    if difficulty == 1:
        # easy
        animation_length = int((0.125 - global.game_distance * 0.025) * track_length)
    if difficulty == 2:
        # medium
        animation_length = int((0.25 - global.game_distance * 0.05) * track_length)
    if difficulty == 3:
        # hard
        animation_length = int((0.375 - global.game_distance * 0.075) * track_length)
    if current_event:
        # modification through event
        animation_length = int(animation_length * current_event.modificator)
        current_event = null
    return animation_length


func move_sprite(animation_length):
    var animation_team = current_team.sprite.get_child(0)
    var animation = animation_team.get_animation("move")
    var animation_path = animation.track_get_path(0)
    var position_x = current_team.sprite.get_parent().position.x
    
    var meters = current_team.meters + animation_length
    if meters >= track_length:
        animation_length = track_length - current_team.meters + 40
        game_stage = "victory"
    current_team.meters += animation_length
    animation.track_set_path(0, animation_path)
    animation.track_insert_key(0, 0, position_x)
    animation.track_insert_key(0, 2, position_x + animation_length)
    animation_team.play("move")
    if animation_length:
        current_team.sprite.play("run")
        play_sound(current_team.sound)


func play_sound(track):
    if global.sounds:
        audio_stream.stream = track["path"]
        audio_stream.volume_db = track["db"]
        audio_stream.play()


func show_player_icon():
    var texture = load(current_team.sprite.sprite_frames.get_frame_texture("idle", 0).resource_path)
    icon_player.texture = texture
    icon_player.show()


func show_player_feedback(client_index, right_answer):
    if client_index == 0:
        if right_answer:
            rect_feedback1.color = Color(0, 0.38, 0, 1)
            rect_feedback1.get_child(0).text = ""
        else:
            rect_feedback1.color = Color(0.38, 0, 0, 1)
            rect_feedback1.get_child(0).text = ""
        hbox_feedback1.show()
    if client_index == 1:
        if right_answer:
            rect_feedback2.color = Color(0, 0.38, 0, 1)
            rect_feedback2.get_child(0).text = ""
        else:
            rect_feedback2.color = Color(0.38, 0, 0, 1)
            rect_feedback2.get_child(0).text = ""
        hbox_feedback2.show()
    if client_index == 2:
        if right_answer:
            rect_feedback3.color = Color(0, 0.38, 0, 1)
            rect_feedback3.get_child(0).text = ""
        else:
            rect_feedback3.color = Color(0.38, 0, 0, 1)
            rect_feedback3.get_child(0).text = ""
        hbox_feedback3.show()
    if client_index == 3:
        if right_answer:
            rect_feedback4.color = Color(0, 0.38, 0, 1)
            rect_feedback4.get_child(0).text = ""
        else:
            rect_feedback4.color = Color(0.38, 0, 0, 1)
            rect_feedback4.get_child(0).text = ""
        hbox_feedback4.show()


func hide_or_show_nodes(nodes_to_hide, nodes_to_show):
    for node in nodes_to_hide:
        if node.get_parent().is_class("TextureButton"):
            node = node.get_parent()
        node.hide()
    for node in nodes_to_show:
        if node.get_parent().is_class("TextureButton"):
            node = node.get_parent()
        node.show()
