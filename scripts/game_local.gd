class_name GameLocal
extends Node2D


const track_length = 1600

var current_countdown
var current_event
var current_player
var current_task
var current_team
var difficulty
var events_start
var events_meantime
var events_end
var game_stage
var modified_meters
var modified_team
var players
var tasks_easy
var tasks_medium
var tasks_hard
var teams

var event_labels_bad = []
var event_labels_good = []
var sound_event_bad = {"path": load("res://assets/sounds/event_bad.wav"), "db": -4}
var sound_event_good = {"path": load("res://assets/sounds/event_good.wav"), "db": -8}
var sound_game_start = {"path": load("res://assets/sounds/game_start.wav"), "db": -4}
var sound_victory = {"path": load("res://assets/sounds/victory.wav"), "db": -6}


@onready var texture_background = $TextureBackground
@onready var area_team1 = $AreaTeam1
@onready var area_team2 = $AreaTeam2
@onready var area_team3 = $AreaTeam3
@onready var area_team4 = $AreaTeam4
@onready var sprite_team1 = $AreaTeam1/SpriteTeam1
@onready var sprite_team2 = $AreaTeam2/SpriteTeam2
@onready var sprite_team3 = $AreaTeam3/SpriteTeam3
@onready var sprite_team4 = $AreaTeam4/SpriteTeam4
@onready var audio_stream = $AudioStreamSoundPlayer
@onready var timer_countdown = $TimerCountdown

@onready var timer_game_start = $GameStart/TimerGameStart
@onready var animation_game_start = $GameStart/AnimationGameStart
@onready var confetti = $GameEnd/Confetti
@onready var animation_game_end = $GameEnd/AnimationGameEnd
@onready var snow = $Snow

@onready var titlebar = $MarginTitlebar/HBoxTitlebar
@onready var icon_player = titlebar.get_node("IconPlayer")
@onready var label_player = titlebar.get_node("LabelPlayer")
@onready var button_menu = titlebar.get_node("ButtonMenu")
@onready var vbox_main = $TexturePanel/MarginMain/HBoxMain/VBoxMain
@onready var label_main = vbox_main.get_node("LabelMain")
@onready var label_answer = vbox_main.get_node("LabelAnswer")
@onready var hbox_top = $TexturePanel/MarginMain/HBoxMain/VBoxMain/HBoxTop
@onready var label_topic = hbox_top.get_node("LabelTopic")
@onready var label_category = hbox_top.get_node("LabelCategory")
@onready var label_difficulty = hbox_top.get_node("LabelDifficulty")

@onready var hbox_bottom = $TexturePanel/MarginMain/HBoxMain/VBoxMain/HBoxBottom
@onready var button_next = hbox_bottom.get_node("ButtonNext/LabelButtonNext")
@onready var button_easy = hbox_bottom.get_node("ButtonEasy/LabelButtonEasy")
@onready var button_medium = hbox_bottom.get_node("ButtonMedium/LabelButtonMedium")
@onready var button_hard = hbox_bottom.get_node("ButtonHard/LabelButtonHard")
@onready var button_team1 = hbox_bottom.get_node("ButtonTeam1/IconButtonTeam1")
@onready var button_team2 = hbox_bottom.get_node("ButtonTeam2/IconButtonTeam2")
@onready var button_team3 = hbox_bottom.get_node("ButtonTeam3/IconButtonTeam3")
@onready var button_team4 = hbox_bottom.get_node("ButtonTeam4/IconButtonTeam4")
@onready var button_right = hbox_bottom.get_node("ButtonRight/LabelButtonRight")
@onready var button_wrong = hbox_bottom.get_node("ButtonWrong/LabelButtonWrong")

@onready var popup_relocate = $PopupRelocate
@onready var hbox_relocate = popup_relocate.get_node("PanelRelocate/MarginRelocate/HBoxRelocate")
@onready var button_backward = hbox_relocate.get_node("ButtonBackward")
@onready var button_forward = hbox_relocate.get_node("ButtonForward")
@onready var popup_settings = $PopupSettings


func _on_ready():
    connect_menu_button_popup()
    set_background_texture()
    set_sprites_and_frames()
    set_teams_and_players()
    set_tasks()
    set_events()
    if global.music:
        music_player.stop()
    timer_game_start.start()


func _on_menu_item_pressed(id):
    if id == 0:
        # New Game
        scene_transition.change_scene("res://scenes/setup_local.tscn")
    if id == 1:
        # Settings
        global.last_scene = "game_local"
        var scene = load("res://scenes/settings.tscn").instantiate()
        popup_settings.add_child(scene)
        popup_settings.popup()
    if id == 2:
        # Main Menu
        scene_transition.change_scene("res://scenes/main.tscn")
    if id == 3:
        # Quit Game
        music_player.stop()
        scene_transition.fade_out()


func _on_button_next_pressed():
    if game_stage == "player_selection":
        return stage_player_selection()
    if game_stage == "difficulty":
        return stage_difficulty()
    if game_stage == "answer":
        return stage_answer()
    if game_stage == "event":
        return stage_movement()
    if game_stage == "victory":
        scene_transition.change_scene("res://scenes/setup_local.tscn")


func _on_button_difficulty_pressed(button_number, button_text):
    difficulty = button_number
    stage_question(button_text)


func _on_button_team_pressed(team_number):
    get_current_team(team_number)
    if global.events:
        var random_number = randi_range(0, (1 / global.events) - 1)
        if random_number == 0:
            return stage_event()
    current_event = null
    stage_movement()


func _on_button_answer_pressed(right_answer):
    if global.game_mode == "team":
        game_stage = "player_selection"
        return stage_player_selection()
    
    if right_answer:
        if global.events:
            var random_number = randi_range(0, (1 / global.events) - 1)
            if random_number == 0:
                return stage_event()
        current_event = null
        stage_movement()
    else:
        stage_difficulty()


func _on_timer_countdown_timeout():
    current_countdown -= 1
    if current_countdown > 0:
        label_main.text = "%s …" % str(current_countdown)
        timer_countdown.start()
    else:
        current_countdown = global.countdown_length
        label_main.add_theme_font_size_override("font_size", 36)
        timer_countdown.stop()
        if game_stage == "player_selection":
            return stage_difficulty()
        if game_stage == "question":
            return get_question()
        if game_stage == "event":
            get_event()
            get_event_button_label()
            return


func _on_timer_game_start_timeout():
    var animation = animation_game_start.get_animation("come_in")
    if global.team_number == 2:
        var start_x = animation.track_get_key_value(2, 0)
        var end_x = animation.track_get_key_value(2, 1)
        animation.track_set_key_value(1, 0, start_x)
        animation.track_set_key_value(1, 1, end_x)
    if global.team_number > 2:
        # enable animations for sprite of team3
        animation.track_set_enabled(6, true)
    if global.team_number > 3:
        # enable animations for sprite of team4
        animation.track_set_enabled(7, true)
    animation_game_start.play("come_in")
    play_sound(sound_game_start)


func _on_animation_game_start_finished(_animation_name):
    button_menu.disabled = false
    button_next.get_parent().disabled = false
    music_player.background_music.shuffle()
    music_player.play(music_player.background_music[0])


func _on_animation_game_end_finished(_animation_name):
    button_next.get_parent().disabled = false
    confetti.show()


func _on_animation_team_finished(_animation_name):
    current_team.sprite.play("idle")
    if game_stage == "victory":
        return await stage_victory()
    if global.game_mode == "player":
        return stage_difficulty()
    
    label_player.text = ""
    label_topic.text = tr("Next Round")
    button_next.text = tr("Next Player")
    var nodes_to_hide = [label_category, label_difficulty, label_main, label_answer]
    var nodes_to_show = [button_next]
    hide_or_show_nodes(nodes_to_hide, nodes_to_show)


func _on_area_team_input_event(_viewport, event, _shape_idx, team_number):
    if not global.cheat_mode:
        return
    
    if event is InputEventMouseButton and event.button_index == 2 and event.is_pressed():
        modified_meters = int((0.125 - global.game_distance * 0.025) * track_length)
        modified_team = get_modified_team(team_number)
        if modified_team.meters - modified_meters < 0:
            button_backward.disabled = true
        else:
            button_backward.disabled = false
        if modified_team.meters + modified_meters >= track_length:
            button_forward.disabled = true
        else:
            button_forward.disabled = false
        
        popup_relocate.position.x = event.position.x
        popup_relocate.position.y = event.position.y
        popup_relocate.popup()


func _on_button_backward_or_forward_pressed(direction):
    modified_meters = modified_meters * direction
    var area_team = modified_team.sprite.get_parent()
    area_team.position.x += modified_meters
    modified_team.meters += modified_meters
    popup_relocate.hide()


func connect_menu_button_popup():
    var popup = button_menu.get_popup()
    popup.add_theme_constant_override("item_start_padding", 8)
    popup.add_theme_constant_override("item_end_padding", 8)
    popup.add_theme_constant_override("h_separation", 8)
    popup.add_theme_constant_override("v_separation", 8)
    popup.add_theme_color_override("font_color", Color(1, 1, 1, 1))
    popup.add_theme_color_override("font_separator_color", Color(1, 1, 1, 1))
    popup.add_theme_font_size_override("font_size", 30)
    popup.id_pressed.connect(_on_menu_item_pressed)


func set_background_texture():
    if global.background == "random":
        var backgrounds = ["desert", "forest", "grassland"]
        var index = randi() % len(backgrounds)
        global.background = backgrounds[index]
    if global.background == "desert":
        texture_background.texture = global.background_desert
    if global.background == "forest":
        texture_background.texture = global.background_forest
    if global.background == "grassland":
        texture_background.texture = global.background_grassland
    if Time.get_date_dict_from_system()["month"] == 12:
        snow.show()
    

func set_sprites_and_frames():
    global.team1.sprite = sprite_team1
    global.team2.sprite = sprite_team2
    global.team3.sprite = sprite_team3
    global.team4.sprite = sprite_team4
    sprite_team1.sprite_frames = global.team1.frames
    sprite_team2.sprite_frames = global.team2.frames
    sprite_team3.sprite_frames = global.team3.frames
    sprite_team4.sprite_frames = global.team4.frames
    
    for sprite in [sprite_team1, sprite_team2, sprite_team3, sprite_team4]:
        if sprite.sprite_frames:
            var frame_path = sprite.sprite_frames.resource_path
            if "adventure" in frame_path or "ninja" in frame_path or "robot" in frame_path:
                sprite.set_offset(Vector2(-13, 0))
            if "zombie" in frame_path:
                sprite.set_offset(Vector2(10, 0))


func set_teams_and_players():
    teams = [global.team1, global.team2]
    sprite_team1.play("idle")
    sprite_team2.play("idle")
    var texture1 = sprite_team1.sprite_frames.get_frame_texture("idle", 0).resource_path
    var texture2 = sprite_team2.sprite_frames.get_frame_texture("idle", 0).resource_path
    button_team1.texture = load(texture1)
    button_team2.texture = load(texture2)
    if global.team_number == 2:
        area_team2.position = area_team3.position
    if global.team_number > 2:
        teams.append(global.team3)
        area_team3.show()
        sprite_team3.play("idle")
        var texture3 = sprite_team3.sprite_frames.get_frame_texture("idle", 0).resource_path
        button_team3.texture = load(texture3)
    if global.team_number > 3:
        teams.append(global.team4)
        area_team4.show()
        sprite_team4.play("idle")
        var texture4 = sprite_team4.sprite_frames.get_frame_texture("idle", 0).resource_path
        button_team4.texture = load(texture4)
    for team in teams:
        team.meters = 0
    
    players = []
    if global.game_mode == "team":
        label_main.text = tr("Be the first team to reach the finish line!")
        for player_number in range(1, global.player_number + 1):
            players.append(player_number)
        game_stage = "player_selection"
    else:
        label_main.text = tr("Be the first player to reach the finish line!")
        game_stage = "difficulty"


func set_tasks():
    tasks_easy = global.tasks_easy.duplicate(true)
    tasks_medium = global.tasks_medium.duplicate(true)
    tasks_hard = global.tasks_hard.duplicate(true)


func set_events():
    events_start = global.events_start.duplicate(true)
    events_meantime = global.events_meantime.duplicate(true)
    events_end = global.events_end.duplicate(true)
    
    event_labels_bad = [
        tr("Damn it!"),
        tr("How disappointing!"),
        tr("Oh dear!"),
        tr("Too bad!"),
        tr("What a pity!")]
    event_labels_good = [
        tr("Awesome!"),
        tr("Fantastic!"),
        tr("How great!"),
        tr("Sensational!"),
        tr("That's amazing!")]


func stage_player_selection():
    label_player.text = ""
    label_topic.text = tr("Player selection")
    
    var nodes_to_hide = [label_category, label_difficulty, label_answer, button_next,
            button_team1, button_team2, button_team3, button_team4, button_wrong]
    var nodes_to_show = [label_main]
    hide_or_show_nodes(nodes_to_hide, nodes_to_show)
    start_countdown()


func stage_difficulty():
    label_topic.text = tr("Difficulty")
    get_next_team()
    if global.game_mode == "team":
        get_next_player()
        var main_text = tr("Player %s of Team \"%s\":\nChoose the difficulty of the next question!")
        label_player.text = tr("Player %s") % str(current_player)
        label_main.text = main_text % [current_player, current_team.name]
    else:
        show_player_icon()
        var player_text = tr("Player %s: %s")
        label_player.text = player_text % [str(current_team.number), current_team.name]
        label_main.text = tr("%s:\nChoose the difficulty of the next question!") \
                % current_team.name
    
    game_stage = "question"
    var nodes_to_hide = [label_category, label_difficulty, label_answer, button_next,
            button_right, button_wrong]
    var nodes_to_show = [label_main, button_easy, button_medium, button_hard]
    hide_or_show_nodes(nodes_to_hide, nodes_to_show)


func stage_question(difficulty_text):
    label_topic.text = tr("Question")
    label_difficulty.text = difficulty_text
    button_next.text = tr("Show Answer")
    
    game_stage = "question"
    var nodes_to_hide = [button_easy, button_medium, button_hard]
    hide_or_show_nodes(nodes_to_hide, [])
    
    if global.game_mode == "team":
        start_countdown()
    else:
        get_question()


func stage_answer():
    var nodes_to_hide
    var nodes_to_show
    label_topic.text = tr("Answer")
    game_stage = "movement"
    
    if global.game_mode == "team":
        nodes_to_hide = [button_next]
        nodes_to_show = [label_answer, button_team1, button_team2, button_wrong]
        if global.team_number > 2:
            nodes_to_show.append(button_team3)
        if global.team_number > 3:
            nodes_to_show.append(button_team4)
    else:
        nodes_to_hide = [button_next]
        nodes_to_show = [label_answer, button_right, button_wrong]
    hide_or_show_nodes(nodes_to_hide, nodes_to_show)


func stage_movement():
    var animation_length = get_animation_length()
    if global.game_mode == "team":
        game_stage = "player_selection"
    else:
        game_stage = "difficulty"
    move_sprite(animation_length)


func stage_event():
    label_topic.text = tr("Event")
    
    game_stage = "event"
    var nodes_to_hide = [label_category, label_difficulty, label_answer, button_next, button_team1,
            button_team2, button_team3, button_team4, button_right, button_wrong]
    hide_or_show_nodes(nodes_to_hide, [])
    start_countdown()


func stage_victory(): 
    label_topic.text = tr("Victory")
    label_main.add_theme_color_override("font_color", Color(0.5, 0, 0, 1))
    label_main.text = tr("The winner is:\n%s!") % current_team.name
    label_answer.add_theme_color_override("font_color", Color(0, 0, 0, 1))
    label_answer.text = tr("Congratulations!")
    button_next.text = tr("Rematch!")
    button_next.get_parent().disabled = true
    if global.game_mode == "team":
        label_player.text = ""

    var nodes_to_hide = [label_category, label_difficulty]
    hide_or_show_nodes(nodes_to_hide, [])

    animation_game_end.play("fade_in")
    music_player.stop()
    var sprites = [sprite_team1, sprite_team2, sprite_team3, sprite_team4]
    sprites.shuffle()
    for sprite in sprites:
        if sprite.get_parent().is_visible() and not sprite == current_team.sprite:
            var timer_length = randf_range(0.2, 0.5)
            await get_tree().create_timer(timer_length).timeout
            sprite.play("dead")
    play_sound(sound_victory)


func start_countdown():
    current_countdown = global.countdown_length
    label_main.text = "%s …" % current_countdown
    label_main.add_theme_font_size_override("font_size", 72)
    timer_countdown.start()


func get_current_team(team_number):
    if team_number == 1:
        current_team = global.team1
    if team_number == 2:
        current_team = global.team2
    if team_number == 3:
        current_team = global.team3
    if team_number == 4:
        current_team = global.team4


func get_next_player():
    var index = randi() % len(players)
    current_player = players[index]
    players.erase(current_player)
    if players == []:
        for number in range(1, global.player_number + 1):
            players.append(number)


func get_next_team():
    if global.game_mode == "team":
        var index = randi() % len(teams)
        current_team = teams[index]
    else:
        current_team = teams[0]
    teams.erase(current_team)
    if teams == []:
        teams = [global.team1, global.team2]
        if global.team_number > 2:
            teams.append(global.team3)
        if global.team_number > 3:
            teams.append(global.team4)


func get_question():
    var index
    if difficulty == 1:
        index = randi() % len(tasks_easy)
        current_task = tasks_easy[index]
        tasks_easy.erase(current_task)
        if tasks_easy == []:
            tasks_easy = global.tasks_easy.duplicate(true)
    if difficulty == 2:
        index = randi() % len(tasks_medium)
        current_task = tasks_medium[index]
        tasks_medium.erase(current_task)
        if tasks_medium == []:
            tasks_medium = global.tasks_medium.duplicate(true)
    if difficulty == 3:
        index = randi() % len(tasks_hard)
        current_task = tasks_hard[index]
        tasks_hard.erase(current_task)
        if tasks_hard == []:
            tasks_hard = global.tasks_hard.duplicate(true)
 
    label_category.text = current_task.category
    label_main.text = current_task.question
    label_answer.text = current_task.answer
    button_next.text = tr("Show Answer")
    
    game_stage = "answer"
    var nodes_to_show = [label_category, label_difficulty, button_next]
    hide_or_show_nodes([], nodes_to_show)


func get_event():
    var random_number
    if current_team.meters == 0:
        random_number = randi() % len(events_start)
        current_event = events_start[random_number]
        events_start.erase(current_event)
        if events_start == []:
            events_start = global.events_start.duplicate(true)
    elif current_team.meters > 1200:
        random_number = randi() % len(events_end)
        current_event = events_end[random_number]
        events_end.erase(current_event)
        if events_end == []:
            events_end = global.events_end.duplicate(true)
    else:
        random_number = randi() % len(events_meantime)
        current_event = events_meantime[random_number]
        events_meantime.erase(current_event)
        if events_meantime == []:
            events_meantime = global.events_meantime.duplicate(true)

    label_main.text = current_event.text
    hide_or_show_nodes([], [button_next])


func get_event_button_label():
    var random_number
    var text
    if current_event.modificator < 1:
        random_number = randi() % len(event_labels_bad)
        text = event_labels_bad[random_number]
        play_sound(sound_event_bad)
    elif current_event.modificator > 1:
        random_number = randi() % len(event_labels_good)
        text = event_labels_good[random_number]
        play_sound(sound_event_good)
    else:
        text = tr("Okay")
    button_next.text = text


func get_animation_length():
    var animation_length
    if difficulty == 1:
        # easy
        animation_length = int((0.125 - global.game_distance * 0.025) * track_length)
    if difficulty == 2:
        # medium
        animation_length = int((0.25 - global.game_distance * 0.05) * track_length)
    if difficulty == 3:
        # hard
        animation_length = int((0.375 - global.game_distance * 0.075) * track_length)
    if current_event:
        # modification through event
        animation_length = int(animation_length * current_event.modificator)
    return animation_length


func move_sprite(animation_length):
    var animation_team = current_team.sprite.get_child(0)
    var animation = animation_team.get_animation("move")
    var animation_path = animation.track_get_path(0)
    var position_x = current_team.sprite.get_parent().position.x
    
    var meters = current_team.meters + animation_length
    if meters >= track_length:
        animation_length = track_length - current_team.meters + 40
        game_stage = "victory"
    current_team.meters += animation_length
    animation.track_set_path(0, animation_path)
    animation.track_insert_key(0, 0, position_x)
    animation.track_insert_key(0, 2, position_x + animation_length)
    animation_team.play("move")
    if animation_length:
        current_team.sprite.play("run")
        play_sound(current_team.sound)

    var nodes_to_hide = [button_next, button_team1, button_team2, button_team3, button_team4,
            button_right, button_wrong]
    hide_or_show_nodes(nodes_to_hide, [])


func play_sound(track):
    if global.sounds:
        audio_stream.stream = track["path"]
        audio_stream.volume_db = track["db"]
        audio_stream.play()


func show_player_icon():
    var texture = load(current_team.sprite.sprite_frames.get_frame_texture("idle", 0).resource_path)
    icon_player.texture = texture
    icon_player.show()


func hide_or_show_nodes(nodes_to_hide, nodes_to_show):
    for node in nodes_to_hide:
        if node.get_parent().is_class("TextureButton"):
            node = node.get_parent()
        node.hide()
    for node in nodes_to_show:
        if node.get_parent().is_class("TextureButton"):
            node = node.get_parent()
        node.show()


func get_modified_team(team_number):
    if team_number == 1:
        return global.team1
    if team_number == 2:
        return global.team2
    if team_number == 3:
        return global.team3
    if team_number == 4:
        return global.team4
