class_name Main
extends Control


@onready var texture_background = $TextureBackground
@onready var vbox_buttons = $MarginMain/HBoxMain/VBoxButtons
@onready var texture_logo = $MarginMain/HBoxMain/TextureLogo
@onready var margin_bottom = $MarginBottom
@onready var label_version = margin_bottom.get_node("HBoxBottom/LabelVersion")
@onready var label_copyright = margin_bottom.get_node("HBoxBottom/LabelCopyright")
@onready var animation_start = $AnimationStart


func _on_ready():
    label_version.text = tr("Version: %s") % ProjectSettings.get_setting("application/config/version")
    
    if global.game_start:
        fly_in_gui()
        if OS.get_name() == "Android":
            OS.request_permissions()
            get_tree().set_quit_on_go_back(false)
    
    if Time.get_date_dict_from_system()["month"] == 12:
        texture_background.texture = load("res://assets/textures/background_main_xmas.jpg")
        texture_logo.texture = load("res://assets/icons/properate_xmas.png")
        for label in [label_version, label_copyright]:
            label.add_theme_color_override("font_color", Color(0, 0, 0, 1))
            
    if global.music:
        if not music_player.audio_stream.playing:
            return music_player.play(music_player.main_theme)
        if music_player.current_track != music_player.main_theme:
            music_player.stop()
            music_player.current_track = music_player.main_theme


func _on_button_play_game_pressed():
    scene_transition.change_scene("res://scenes/setup.tscn")


func _on_button_editor_pressed():
    scene_transition.change_scene("res://scenes/editor.tscn")


func _on_button_settings_pressed():
    global.last_scene = "main"
    scene_transition.change_scene("res://scenes/settings.tscn")


func _on_button_credits_pressed():
    scene_transition.change_scene("res://scenes/credits.tscn")


func _on_button_quit_pressed():
    music_player.stop()
    scene_transition.fade_out()


func _on_animation_start_finished(_animation_name):
    for button in vbox_buttons.get_children():
        button.disabled = false
    margin_bottom.show()


func fly_in_gui():
    global.game_start = false
    for button in vbox_buttons.get_children():
        button.disabled = true
    margin_bottom.hide()
    animation_start.play("fly_in")
