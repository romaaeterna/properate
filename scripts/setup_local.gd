class_name SetupLocal
extends Control


var current_background = "random"
var current_sprite_frames
var java_script_callback = null
var json_filter = PackedStringArray(["*.json ; JSON Files"])
var tasks_dict = {}

var sprite_frames = [
    load("res://tres/sprites_cat.tres"),
    load("res://tres/sprites_dog.tres"),
    load("res://tres/sprites_adventure_boy.tres"),
    load("res://tres/sprites_ninja_girl.tres"),
    load("res://tres/sprites_knight.tres"),
    load("res://tres/sprites_robot.tres"),
    load("res://tres/sprites_zombie_girl.tres"),
    load("res://tres/sprites_zombie_boy.tres")]
var sound_running_adventure_boy = {"path": load("res://assets/sounds/running_adventure_boy.wav"), "db": 4}
var sound_running_cat = {"path": load("res://assets/sounds/running_cat.wav"), "db": 8}
var sound_running_dog = {"path": load("res://assets/sounds/running_dog.wav"), "db": -10}
var sound_running_knight = {"path": load("res://assets/sounds/running_knight.wav"), "db": 4}
var sound_running_ninja_girl = {"path": load("res://assets/sounds/running_ninja_girl.wav"), "db": 2}
var sound_running_robot = {"path": load("res://assets/sounds/running_robot.wav"), "db": -4}
var sound_running_zombie_boy = {"path": load("res://assets/sounds/running_zombie_boy.wav"), "db": 0}
var sound_running_zombie_girl = {"path": load("res://assets/sounds/running_zombie_girl.wav"), "db": -4}

@onready var vbox_setup = $VBoxSetupLocal
@onready var background_window = vbox_setup.get_node("TextureBackground")
@onready var hbox_mode = vbox_setup.get_node("MarginTop/CenterMode/HBoxMode")
@onready var button_team_mode = hbox_mode.get_node("ButtonTeamMode")
@onready var button_player_mode = hbox_mode.get_node("ButtonPlayerMode")
@onready var hbox_main = vbox_setup.get_node("TextureBackground/MarginMain/HBoxMain")
@onready var vbox_left = hbox_main.get_node("PanelLeft/MarginLeft/VBoxLeft")
@onready var label_file = vbox_left.get_node("VBoxFile/HBoxFile/LabelFile")
@onready var tree_units = vbox_left.get_node("VBoxFile/TreeUnits")
@onready var vbox_player = vbox_left.get_node("VBoxPlayer")
@onready var label_player_number = vbox_player.get_node("HBoxPlayer/LabelPlayerNumber")
@onready var slider_player_number = vbox_player.get_node("HBoxPlayer/SliderPlayerNumber")
@onready var hbox_distance = vbox_left.get_node("VBoxDistance/HBoxDistance")
@onready var button_short = hbox_distance.get_node("ButtonShort")
@onready var button_normal = hbox_distance.get_node("ButtonNormal")
@onready var button_long = hbox_distance.get_node("ButtonLong")
@onready var hbox_background = vbox_left.get_node("VBoxBackground/PanelBackground/HBoxBackground")
@onready var background_preview = hbox_background.get_node("CenterBackground/TextureBackground")
@onready var background_label = hbox_background.get_node("CenterBackground/TextureBackground/BackgroundLabel")

@onready var grid_right = hbox_main.get_node("PanelRight/MarginRight/GridRight")
@onready var check_button_team1 = grid_right.get_node("VBoxTeam1/CheckButton1")
@onready var check_button_team2 = grid_right.get_node("VBoxTeam2/CheckButton2")
@onready var check_button_team3 = grid_right.get_node("VBoxTeam3/CheckButton3")
@onready var check_button_team4 = grid_right.get_node("VBoxTeam4/CheckButton4")
@onready var line_team1 = grid_right.get_node("VBoxTeam1/HBoxName1/LineName1")
@onready var line_team2 = grid_right.get_node("VBoxTeam2/HBoxName2/LineName2")
@onready var line_team3 = grid_right.get_node("VBoxTeam3/HBoxName3/LineName3")
@onready var line_team4 = grid_right.get_node("VBoxTeam4/HBoxName4/LineName4")
@onready var label_team3 = grid_right.get_node("VBoxTeam3/HBoxName3/LabelName3")
@onready var label_team4 = grid_right.get_node("VBoxTeam4/HBoxName4/LabelName4")
@onready var panel_sprite_team3 = grid_right.get_node("VBoxTeam3/PanelSprite3")
@onready var panel_sprite_team4 = grid_right.get_node("VBoxTeam4/PanelSprite4")
@onready var button_sprite_team3_left = grid_right.get_node("VBoxTeam3/PanelSprite3/HBoxSprite3/ButtonSprite3Left")
@onready var button_sprite_team3_right = grid_right.get_node("VBoxTeam3/PanelSprite3/HBoxSprite3/ButtonSprite3Right")
@onready var button_sprite_team4_left = grid_right.get_node("VBoxTeam4/PanelSprite4/HBoxSprite4/ButtonSprite4Left")
@onready var button_sprite_team4_right = grid_right.get_node("VBoxTeam4/PanelSprite4/HBoxSprite4/ButtonSprite4Right")
@onready var sprite_team1 = grid_right.get_node("VBoxTeam1/PanelSprite1/HBoxSprite1/TextureSprite1/SpriteTeam1")
@onready var sprite_team2 = grid_right.get_node("VBoxTeam2/PanelSprite2/HBoxSprite2/TextureSprite2/SpriteTeam2")
@onready var sprite_team3 = grid_right.get_node("VBoxTeam3/PanelSprite3/HBoxSprite3/TextureSprite3/SpriteTeam3")
@onready var sprite_team4 = grid_right.get_node("VBoxTeam4/PanelSprite4/HBoxSprite4/TextureSprite4/SpriteTeam4")

@onready var message_dialog = $MessageDialog
@onready var vbox_dialog = $MessageDialog/MarginDialog/TextureDialog/MarginContent/VBoxContent
@onready var button_dialog = vbox_dialog.get_node("ButtonDialog")
@onready var label_dialog = vbox_dialog.get_node("LabelDialog")
@onready var file_dialog = null


func _on_ready():
    set_process(false)
    build_file_dialog()
    setup_game_mode()
    setup_left_panel()
    setup_right_panel()
    if global.music and music_player.current_track != music_player.main_theme:
        music_player.stop()
        music_player.current_track = music_player.main_theme


func _process(_delta):
    if DisplayServer.virtual_keyboard_get_height():
        vbox_setup.position.y = DisplayServer.virtual_keyboard_get_height() * -1
    else:
        vbox_setup.position.y = 0


func _on_button_start_pressed():  
    var error_text_task = get_tasks()
    if error_text_task:
        show_error_dialog(error_text_task)
        return
    var error_text_names = check_names()
    if error_text_names:
        show_error_dialog(error_text_names)
        return
    
    set_team_frames_and_sounds()
    scene_transition.change_scene("res://scenes/game_local.tscn")


func _on_button_game_mode_toggled(_pressed):
    if button_team_mode.button_pressed:
        global.game_mode = "team"
        vbox_player.show()
        check_button_team1.text = tr("Team 1")
        check_button_team2.text = tr("Team 2")
        check_button_team3.text = tr("Team 3")
        check_button_team4.text = tr("Team 4")
    if button_player_mode.button_pressed:
        global.game_mode = "player"
        vbox_player.hide()
        check_button_team1.text = tr("Player 1")
        check_button_team2.text = tr("Player 2")
        check_button_team3.text = tr("Player 3")
        check_button_team4.text = tr("Player 4")


func _on_button_back_pressed():
    scene_transition.change_scene("res://scenes/main.tscn")


func _on_button_file_pressed():
    if OS.get_name() in ["Linux", "macOS", "Windows"]:
        var current_dir = ""
        if FileAccess.file_exists(global.last_file):
            current_dir = global.last_file.get_base_dir()
        elif global.default_dir and DirAccess.dir_exists_absolute(global.default_dir):
            current_dir = global.default_dir
        var title = tr("Select a File")
        var mode = DisplayServer.FILE_DIALOG_MODE_OPEN_FILE
        DisplayServer.file_dialog_show(title, current_dir, "", false, mode, json_filter,
            _on_native_file_dialog_file_selected)
    elif OS.get_name() == "Web":
        JavaScriptBridge.eval("openFile()")
    else:
        if FileAccess.file_exists(global.last_file):
            file_dialog.current_dir = global.last_file.get_base_dir()
        elif global.default_dir and DirAccess.dir_exists_absolute(global.default_dir):
            file_dialog.current_dir = global.default_dir
        file_dialog.popup_centered(Vector2i(1600, 900))


func _on_button_distance_pressed(distance):
    global.game_distance = distance


func _on_slider_player_value_changed(value):
    global.player_number = value
    label_player_number.text = str(value)


func _on_button_background_left_pressed():
    if current_background == "random":
        return change_background("desert", global.background_desert, "")
    if current_background == "desert":
        return change_background("grassland", global.background_grassland, "")
    if current_background == "grassland":
        return change_background("forest", global.background_forest, "")
    if current_background == "forest":
        return change_background("random", global.background_random, "?")


func _on_button_background_right_pressed():
    if current_background == "random":
        return change_background("forest", global.background_forest, "")
    if current_background == "forest":
        return change_background("grassland", global.background_grassland, "")
    if current_background == "grassland":
        return change_background("desert", global.background_desert, "")
    if current_background == "desert":
        return change_background("random", global.background_random, "?")


func _on_button_team_3_pressed():
    if check_button_team3.button_pressed:
        global.team_number = 3
        line_team3.mouse_filter = MOUSE_FILTER_STOP
        modify_team_3_nodes(1.0, true, false)
        show_sprite(3, sprite_team3, null)
    else:
        global.team_number = 2
        check_button_team4.button_pressed = false
        modify_team_3_nodes(0.4, false, true)
        modify_team_4_nodes(0.4, false, true)
        hide_sprite(3, sprite_team3)
        hide_sprite(4, sprite_team4)


func _on_button_team_4_pressed():
    if check_button_team4.button_pressed:
        global.team_number = 4
        line_team4.mouse_filter = MOUSE_FILTER_STOP
        modify_team_4_nodes(1.0, true, false)
        show_sprite(4, sprite_team4, null)
    else:
        global.team_number = 3
        modify_team_4_nodes(0.4, false, true)
        hide_sprite(4, sprite_team4)


func _on_line_team_1_text_changed(new_name):
    global.team1.name = new_name


func _on_line_team_2_text_changed(new_name):
    global.team2.name = new_name


func _on_line_team_3_text_changed(new_name):
    global.team3.name = new_name


func _on_line_team_4_text_changed(new_name):
    global.team4.name = new_name


func _on_line_team_focus_entered():
    if OS.get_name() == "Android":
        set_process(true)


func _on_line_team_focus_exited():
    if OS.get_name() == "Android":
        vbox_setup.position.y = 0
        set_process(false)


func _on_button_sprite_left_pressed(team_number):
    if team_number == 1:
        get_previous_sprite(1, sprite_team1)
    if team_number == 2:
        get_previous_sprite(2, sprite_team2)
    if team_number == 3:
        get_previous_sprite(3, sprite_team3)
    if team_number == 4:
        get_previous_sprite(4, sprite_team4)


func _on_button_sprite_right_pressed(team_number):
    if team_number == 1:
        get_next_sprite(1, sprite_team1)
    if team_number == 2:
        get_next_sprite(2, sprite_team2)
    if team_number == 3:
        get_next_sprite(3, sprite_team3)
    if team_number == 4:
        get_next_sprite(4, sprite_team4)


func _on_button_dialog_pressed():
    message_dialog.hide()


func _on_native_file_dialog_file_selected(_status, selected_paths, _selected_filter_index):
    if selected_paths.is_empty():
        return
    var file = FileAccess.open(selected_paths[0], FileAccess.READ)
    load_json_data([selected_paths[0], file.get_as_text()])


func _on_file_dialog_file_selected(path):
    path = path[0] if typeof(path) == TYPE_PACKED_STRING_ARRAY else path
    var file = FileAccess.open(path, FileAccess.READ)
    load_json_data([path, file.get_as_text()])


func build_file_dialog():
    if OS.get_name() in ["Linux", "macOS", "Windows"]:
        return
    elif OS.get_name() == "Web":
        java_script_callback = JavaScriptBridge.create_callback(load_json_data)
        JavaScriptBridge.get_interface("web_file_dialog").dataLoaded = java_script_callback
        return
    file_dialog = load("res://scenes/file_dialog.tscn").instantiate()
    file_dialog.cancel_button_text = tr("Cancel")
    file_dialog.ok_button_text = tr("Open")
    file_dialog.access = 2
    file_dialog.file_mode = 0
    file_dialog.set_filters(json_filter)
    file_dialog.file_selected.connect(_on_file_dialog_file_selected)
    file_dialog.files_selected.connect(_on_file_dialog_file_selected)
    add_child(file_dialog)


func setup_game_mode():
    if global.game_mode == "team":
        vbox_player.show()
        check_button_team1.text = tr("Team 1")
        check_button_team2.text = tr("Team 2")
        check_button_team3.text = tr("Team 3")
        check_button_team4.text = tr("Team 4")
    if global.game_mode == "player":
        vbox_player.hide()
        check_button_team1.text = tr("Player 1")
        check_button_team2.text = tr("Player 2")
        check_button_team3.text = tr("Player 3")
        check_button_team4.text = tr("Player 4")


func setup_left_panel():
    if global.game_mode != "team":
        button_player_mode.set_pressed(true)
        _on_button_game_mode_toggled(true)
    if FileAccess.file_exists(global.last_file):
        _on_file_dialog_file_selected(global.last_file)
    if global.game_distance != 2:
        if global.game_distance == 1:
            button_short.set_pressed(true)
        if global.game_distance == 3:
            button_long.set_pressed(true)
    slider_player_number.value = global.player_number


func setup_right_panel():
    if global.team1.frames: sprite_team1.sprite_frames = global.team1.frames
    if global.team2.frames: sprite_team2.sprite_frames = global.team2.frames
    current_sprite_frames = [sprite_team1.sprite_frames, sprite_team2.sprite_frames, null, null]
    line_team1.text = global.team1.name
    line_team2.text = global.team2.name
    sprite_team1.play("idle")
    sprite_team2.play("idle")
    
    if global.team_number > 2:
        check_button_team3.set_pressed(true)
        line_team3.text = global.team3.name
        modify_team_3_nodes(1.0, true, false)
        show_sprite(3, sprite_team3, global.team3.frames)
    
    if global.team_number > 3:
        check_button_team4.set_pressed(true)
        line_team4.text = global.team4.name
        modify_team_4_nodes(1.0, true, false)
        show_sprite(4, sprite_team4, global.team4.frames)


func load_json_data(data):
    var path = data[0]
    var string = data[1]
    var json = JSON.new()
    var error = json.parse(string)
    tree_units.clear()
    
    if error != OK:
        label_file.text = tr("No File Loaded")
        show_error_dialog(tr("ERROR:\nThe file could not be opened!"))
        return
    
    tasks_dict = json.get_data()
    var units = tasks_dict.keys()
    if not check_json_data_structure(units):
        label_file.text = tr("No File Loaded")
        show_error_dialog(tr("ERROR:\nThe file data is incomplete!"))
        return
    
    var root = tree_units.create_item()
    for unit in units:
        var new_unit = tree_units.create_item(root)
        new_unit.set_cell_mode(0, 1)
        new_unit.set_editable(0, true)
        new_unit.set_text(0, unit)
    label_file.text = path.get_file()
    global.last_file = path


func check_json_data_structure(units):
    for unit in units:
        for task in tasks_dict[unit]:
            if not "id" in task or not "category" in task or not "difficulty" in task \
                    or not "question" in task or not "answer" in task:
                return false
    return true


func change_background(bg_name, bg_resource, bg_text):
    global.background = bg_name
    current_background = bg_name
    background_preview.texture = bg_resource
    background_window.texture = bg_resource
    background_label.text = bg_text


func modify_team_3_nodes(alpha, editable, disabled):
    label_team3.add_theme_color_override("font_color", Color(1.0, 1.0, 1.0, alpha))
    line_team3.editable = editable
    panel_sprite_team3.modulate = Color(1.0, 1.0, 1.0, alpha)
    button_sprite_team3_left.disabled = disabled
    button_sprite_team3_right.disabled = disabled
    check_button_team4.disabled = disabled
    if disabled:
        line_team3.mouse_filter = MOUSE_FILTER_IGNORE
        line_team3.release_focus()
    else:
        line_team3.mouse_filter = MOUSE_FILTER_STOP


func modify_team_4_nodes(alpha, editable, disabled):
    label_team4.add_theme_color_override("font_color", Color(1.0, 1.0, 1.0, alpha))
    line_team4.editable = editable
    panel_sprite_team4.modulate = Color(1.0, 1.0, 1.0, alpha)
    button_sprite_team4_left.disabled = disabled
    button_sprite_team4_right.disabled = disabled
    if disabled:
        line_team4.mouse_filter = MOUSE_FILTER_IGNORE
        line_team4.release_focus()
    else:
        line_team4.mouse_filter = MOUSE_FILTER_STOP


func show_sprite(number, sprite, frames):
    if not frames:
        var sprite_frames_unused = []
        for frame in sprite_frames:
            if not frame in current_sprite_frames:
                sprite_frames_unused.append(frame)
        frames = sprite_frames_unused[0]
    sprite.sprite_frames = frames
    sprite.play("idle")
    current_sprite_frames[number - 1] = frames
    sprite.get_parent().texture = null
    sprite.show()


func hide_sprite(number, sprite):
    sprite.sprite_frames = null
    current_sprite_frames[number - 1] = null
    sprite.get_parent().texture = load("res://tres/texture_sprite_empty.tres")
    sprite.hide()


func get_previous_sprite(number, sprite):
    var sprite_frames_unused = []
    for frame in sprite_frames:
        if not frame in current_sprite_frames:
            sprite_frames_unused.append(frame)
    sprite_frames_unused.reverse()
    var index_old = sprite_frames.find(sprite.sprite_frames)
    for frame in sprite_frames_unused:
        var index_new = sprite_frames.find(frame)
        if index_new < index_old:
            sprite.sprite_frames = sprite_frames[index_new]
            sprite.play("idle")
            current_sprite_frames[number - 1] = sprite_frames[index_new]
            return
    sprite.sprite_frames = sprite_frames_unused[0]
    sprite.play("idle")
    current_sprite_frames[number - 1] = sprite_frames_unused[0]


func get_next_sprite(number, sprite):
    var sprite_frames_unused = []
    for frame in sprite_frames:
        if not frame in current_sprite_frames:
            sprite_frames_unused.append(frame)
    var index_old = sprite_frames.find(sprite.sprite_frames)
    for frame in sprite_frames_unused:
        var index_new = sprite_frames.find(frame)
        if index_new > index_old:
            sprite.sprite_frames = sprite_frames[index_new]
            sprite.play("idle")
            current_sprite_frames[number - 1] = sprite_frames[index_new]
            return
    sprite.sprite_frames = sprite_frames_unused[0]
    sprite.play("idle")
    current_sprite_frames[number - 1] = sprite_frames_unused[0]


func get_tasks():
    global.tasks = []
    global.tasks_easy = []
    global.tasks_medium = []
    global.tasks_hard = []
    
    if not tree_units.get_root():
        return tr("Please open a file with tasks!")
    var units_selected = []
    for unit in tree_units.get_root().get_children():
        if unit.is_checked(0):
            units_selected.append(unit.get_text(0))
            for task in tasks_dict[unit.get_text(0)]:
                global.tasks.append(task)
    if units_selected == []:
        return tr("Please select at least one unit!")

    for task in global.tasks:
        if task.difficulty == 1:
            global.tasks_easy.append(task)
        if task.difficulty == 2:
            global.tasks_medium.append(task)
        if task.difficulty == 3:
            global.tasks_hard.append(task)
    if global.tasks_easy == [] or global.tasks_medium == [] or global.tasks_hard == []:
        return tr("Please select units inluding all levels of difficulty!")
    return ""


func set_team_frames_and_sounds():
    global.team1.frames = sprite_team1.sprite_frames
    global.team2.frames = sprite_team2.sprite_frames
    global.team3.frames = sprite_team3.sprite_frames
    global.team4.frames = sprite_team4.sprite_frames

    for team in [global.team1, global.team2, global.team3, global.team4]:
        if "adventure" in team.frames.resource_path:
            team.sound = sound_running_adventure_boy
        elif "cat" in team.frames.resource_path:
            team.sound = sound_running_cat
        elif "dog" in team.frames.resource_path:
            team.sound = sound_running_dog
        elif "knight" in team.frames.resource_path:
            team.sound = sound_running_knight
        elif "ninja" in team.frames.resource_path:
            team.sound = sound_running_ninja_girl
        elif "robot" in team.frames.resource_path:
            team.sound = sound_running_robot
        elif "zombie_boy" in team.frames.resource_path:
            team.sound = sound_running_zombie_boy
        elif "zombie_girl" in team.frames.resource_path:
            team.sound = sound_running_zombie_girl


func check_names():
    var error_text
    if global.game_mode == "team":
        error_text = tr("Please type in a name for each team!")
    else:
        error_text = tr("Please type in a name for each player!")
    if line_team1.text == "" or line_team2.text == "":
        return error_text
    var team_names = [line_team1.text, line_team2.text]
    if global.team_number > 2:
        if line_team3.text == "":
            return error_text
        team_names.append(line_team3.text)
    if global.team_number > 3:
        if line_team4.text == "":
            return error_text
        team_names.append(line_team4.text)
    for team_name in team_names:
        if team_names.count(team_name) > 1:
            if global.game_mode == "team":
                return tr("Please type in different names for each team!")
            else:
                return tr("Please type in different names for each player!")
    return ""


func show_error_dialog(error_text):
    if file_dialog:
        file_dialog.hide()
    label_dialog.text = error_text
    message_dialog.show()
