class_name Settings
extends Control


@onready var grid_content = $VBoxSettings/TextureBackground/MarginMain/PanelMain/MarginContent/GridContent
@onready var button_english = grid_content.get_node("HBoxLanguage/ButtonEnglish")
@onready var button_german = grid_content.get_node("HBoxLanguage/ButtonGerman")
@onready var label_events = grid_content.get_node("LabelEvents")
@onready var hbox_events = grid_content.get_node("HBoxEvents")
@onready var slider_events = hbox_events.get_node("SliderEvents")
@onready var label_events_percent = hbox_events.get_node("LabelEventsPercent")
@onready var label_cheat_mode = grid_content.get_node("LabelCheatMode")
@onready var check_cheat_mode = grid_content.get_node("CheckCheatMode")
@onready var slider_music = grid_content.get_node("HBoxMusic/SliderMusic")
@onready var label_music_percent = grid_content.get_node("HBoxMusic/LabelMusicPercent")
@onready var slider_sounds = grid_content.get_node("HBoxSounds/SliderSounds")
@onready var label_sounds_percent = grid_content.get_node("HBoxSounds/LabelSoundsPercent")
@onready var label_countdown = grid_content.get_node("LabelCountdown")
@onready var hbox_countdown = grid_content.get_node("HBoxCountdown")
@onready var slider_countdown = hbox_countdown.get_node("SliderCountdown")
@onready var label_countdown_number = hbox_countdown.get_node("LabelCountdownNumber")
@onready var label_default_dir = grid_content.get_node("LabelDefaultDir")
@onready var button_default_dir = grid_content.get_node("ButtonDefaultDir")
@onready var file_dialog = null


func _on_ready():
    hide_nodes()
    build_file_dialog()
    apply_settings()


func _on_button_restore_pressed():
    restore_settings()
    apply_settings()


func _on_button_back_pressed():
    save_settings()
    global.load_events()
    if global.last_scene == "game_local":
        var game_local = get_tree().get_root().get_node("GameLocal")
        game_local.set_events()
        game_local.get_node("PopupSettings").hide()
    elif global.last_scene == "game_online":
        var game_online = get_tree().get_root().get_node("GameOnline")
        game_online.set_events()
        game_online.get_node("PopupSettings").hide()
    else:
        scene_transition.change_scene("res://scenes/main.tscn")


func _on_button_language_pressed(language_code):
    global.language = language_code
    TranslationServer.set_locale(language_code)


func _on_slider_events_value_changed(value):
    global.events = value
    label_events_percent.text = "%s %%" % str(value * 100)


func _on_check_cheat_mode_toggled(toggle_mode):
    global.cheat_mode = toggle_mode


func _on_slider_music_value_changed(value):
    global.music = value
    label_music_percent.text = "%s %%" % str(value * 100)
    AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Music"), linear_to_db(value))
    if not global.music:
        return music_player.stop()
    if music_player.animation.is_playing():
        music_player.animation.stop()
    if not music_player.audio_stream.playing and global.last_scene == "main":
        return music_player.play(music_player.main_theme)
    if not music_player.audio_stream.playing and global.last_scene.begins_with("game"):
        return music_player.play(music_player.background_music[0])


func _on_slider_sounds_value_changed(value):
    global.sounds = value
    label_sounds_percent.text = "%s %%" % str(value * 100)
    AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Sounds"), linear_to_db(value))


func _on_slider_countdown_value_changed(value):
    global.countdown_length = value
    label_countdown_number.text = "%s s" % str(value)


func _on_button_default_dir_pressed():
    if OS.get_name() in ["Linux", "macOS", "Windows"]:
        var title = tr("Select a Directory")
        var mode = DisplayServer.FILE_DIALOG_MODE_OPEN_DIR
        var filter = PackedStringArray([])
        DisplayServer.file_dialog_show(title, global.default_dir, "", false, mode, filter,
            _on_native_file_dialog_dir_selected)
    else:
        file_dialog.current_dir = global.default_dir
        file_dialog.popup_centered(Vector2i(1600, 900))


func _on_native_file_dialog_dir_selected(_status, selected_paths, _selected_filter_index):
    if selected_paths.is_empty():
        return
    global.default_dir = selected_paths[0]
    button_default_dir.get_child(0).text = selected_paths[0].replace("\\", "/").split("/")[-1]


func _on_file_dialog_dir_selected(path):
    global.default_dir = path
    button_default_dir.get_child(0).text = path.replace("\\", "/").split("/")[-1]


func hide_nodes():
    if OS.get_name() == "Web":
        label_default_dir.hide()
        button_default_dir.hide()
    if global.last_scene == "game_online":
        label_cheat_mode.hide()
        check_cheat_mode.hide()
        label_countdown.hide()
        hbox_countdown.hide()
        if global.client_index > 0:
            label_events.hide()
            hbox_events.hide()


func build_file_dialog():
    if OS.get_name() in ["Linux", "macOS", "Windows"]:
        return
    file_dialog = load("res://scenes/file_dialog.tscn").instantiate()
    file_dialog.cancel_button_text = tr("Cancel")
    file_dialog.ok_button_text = tr("Select")
    file_dialog.access = 2
    file_dialog.file_mode = 2
    file_dialog.dir_selected.connect(_on_file_dialog_dir_selected)
    add_child(file_dialog)


func restore_settings():
    global.language = OS.get_locale_language()
    global.events = 0.1
    global.cheat_mode = false
    global.music = 1.0
    global.sounds = 1.0
    global.countdown_length = 3
    global.default_dir = global.documents_dir


func apply_settings():
    toggle_language_button()
    slider_events.value = global.events
    check_cheat_mode.button_pressed = global.cheat_mode
    slider_music.value = global.music
    slider_sounds.value = global.sounds
    slider_countdown.value = global.countdown_length
    if global.default_dir == "":
        button_default_dir.get_child(0).text = tr("None")
    else:
        var folder = global.default_dir.split("/")[-1]
        button_default_dir.get_child(0).text = folder


func toggle_language_button():
    if global.language == "de":
        button_german.button_pressed = true
    else:
        global.language = "en"
        button_english.button_pressed = true
    TranslationServer.set_locale(global.language)


func save_settings():
    var config = ConfigFile.new()
    config.set_value("General", "language", global.language)
    config.set_value("General", "events", global.events)
    config.set_value("General", "cheat_mode", global.cheat_mode)
    config.set_value("General", "music", global.music)
    config.set_value("General", "sounds", global.sounds)
    config.set_value("General", "countdown", global.countdown_length)
    config.set_value("General", "default_dir", global.default_dir)
    config.save("user://settings.ini")
