class_name SetupOnline
extends Control


var current_background = "random"
var java_script_callback = null
var json_filter = PackedStringArray(["*.json ; JSON Files"])
var tasks_dict = {}

var message_no_name = tr("Please type in your name and choose your character!")
var message_no_players = tr("Waiting for other players …")
var message_ready = tr("Click \"Ready\" when your are finished!")
var message_ready_all = tr("Waiting for all players to be ready …")
var message_same_frame = tr("Please choose a different character!")
var message_same_name = tr("Please type in a different name!")
var message_start_server = tr("Click \"Start Game\"!")
var message_start_client = tr("Waiting for game start by host …")
var sprite_frames = [
    load("res://tres/sprites_cat.tres"),
    load("res://tres/sprites_dog.tres"),
    load("res://tres/sprites_adventure_boy.tres"),
    load("res://tres/sprites_ninja_girl.tres"),
    load("res://tres/sprites_knight.tres"),
    load("res://tres/sprites_robot.tres"),
    load("res://tres/sprites_zombie_girl.tres"),
    load("res://tres/sprites_zombie_boy.tres")]
var sound_running_adventure_boy = {"path": load("res://assets/sounds/running_adventure_boy.wav"), "db": 4}
var sound_running_cat = {"path": load("res://assets/sounds/running_cat.wav"), "db": 8}
var sound_running_dog = {"path": load("res://assets/sounds/running_dog.wav"), "db": -10}
var sound_running_knight = {"path": load("res://assets/sounds/running_knight.wav"), "db": 4}
var sound_running_ninja_girl = {"path": load("res://assets/sounds/running_ninja_girl.wav"), "db": 2}
var sound_running_robot = {"path": load("res://assets/sounds/running_robot.wav"), "db": -4}
var sound_running_zombie_boy = {"path": load("res://assets/sounds/running_zombie_boy.wav"), "db": 0}
var sound_running_zombie_girl = {"path": load("res://assets/sounds/running_zombie_girl.wav"), "db": -4}

@onready var vbox_setup = $VBoxSetupOnline
@onready var background_window = vbox_setup.get_node("TextureBackground")
@onready var button_start = vbox_setup.get_node("MarginTop/ButtonStart")
@onready var hbox_network = vbox_setup.get_node("MarginTop/CenterNetwork/HBoxNetwork")
@onready var label_network = hbox_network.get_node("LabelNetwork")
@onready var icon_warning = hbox_network.get_node("IconWarning")
@onready var hbox_main = vbox_setup.get_node("TextureBackground/MarginMain/HBoxMain")

@onready var panel_left = hbox_main.get_node("PanelLeft")
@onready var vbox_left = hbox_main.get_node("PanelLeft/MarginLeft/VBoxLeft")
@onready var label_file = vbox_left.get_node("VBoxFile/HBoxFile/LabelFile")
@onready var button_file = vbox_left.get_node("VBoxFile/HBoxFile/ButtonFile")
@onready var tree_units = vbox_left.get_node("VBoxFile/TreeUnits")
@onready var label_distance = vbox_left.get_node("VBoxDistance/LabelDistance")
@onready var hbox_distance = vbox_left.get_node("VBoxDistance/HBoxDistance")
@onready var button_distance_short = hbox_distance.get_node("ButtonDistanceShort")
@onready var button_distance_normal = hbox_distance.get_node("ButtonDistanceNormal")
@onready var button_distance_long = hbox_distance.get_node("ButtonDistanceLong")
@onready var label_timer = vbox_left.get_node("VBoxTimer/LabelTimer")
@onready var hbox_timer = vbox_left.get_node("VBoxTimer/HBoxTimer")
@onready var button_timer_short = hbox_timer.get_node("ButtonTimerShort")
@onready var button_timer_normal = hbox_timer.get_node("ButtonTimerNormal")
@onready var button_timer_long = hbox_timer.get_node("ButtonTimerLong")
@onready var label_background = vbox_left.get_node("VBoxBackground/LabelBackground")
@onready var panel_background = vbox_left.get_node("VBoxBackground/PanelBackground")
@onready var hbox_background = panel_background.get_node("HBoxBackground")
@onready var button_background_left = hbox_background.get_node("ButtonBackgroundLeft")
@onready var button_background_right = hbox_background.get_node("ButtonBackgroundRight")
@onready var background_preview = hbox_background.get_node("CenterBackground/TextureBackground")
@onready var background_label = hbox_background.get_node("CenterBackground/TextureBackground/BackgroundLabel")

@onready var panel_right = hbox_main.get_node("PanelRight")
@onready var grid_right = hbox_main.get_node("PanelRight/MarginRight/GridRight")
@onready var check_button_1 = grid_right.get_node("VBox1/CheckButton1")
@onready var check_button_2 = grid_right.get_node("VBox2/CheckButton2")
@onready var check_button_3 = grid_right.get_node("VBox3/CheckButton3")
@onready var check_button_4 = grid_right.get_node("VBox4/CheckButton4")
@onready var line_1 = grid_right.get_node("VBox1/HBoxName1/Line1")
@onready var line_2 = grid_right.get_node("VBox2/HBoxName2/Line2")
@onready var line_3 = grid_right.get_node("VBox3/HBoxName3/Line3")
@onready var line_4 = grid_right.get_node("VBox4/HBoxName4/Line4")
@onready var label_1 = grid_right.get_node("VBox1/HBoxName1/Label1")
@onready var label_2 = grid_right.get_node("VBox2/HBoxName2/Label2")
@onready var label_3 = grid_right.get_node("VBox3/HBoxName3/Label3")
@onready var label_4 = grid_right.get_node("VBox4/HBoxName4/Label4")
@onready var panel_sprite_1 = grid_right.get_node("VBox1/PanelSprite1")
@onready var panel_sprite_2 = grid_right.get_node("VBox2/PanelSprite2")
@onready var panel_sprite_3 = grid_right.get_node("VBox3/PanelSprite3")
@onready var panel_sprite_4 = grid_right.get_node("VBox4/PanelSprite4")
@onready var button_sprite_1_left = grid_right.get_node("VBox1/PanelSprite1/HBoxSprite1/ButtonSprite1Left")
@onready var button_sprite_1_right = grid_right.get_node("VBox1/PanelSprite1/HBoxSprite1/ButtonSprite1Right")
@onready var button_sprite_2_left = grid_right.get_node("VBox2/PanelSprite2/HBoxSprite2/ButtonSprite2Left")
@onready var button_sprite_2_right = grid_right.get_node("VBox2/PanelSprite2/HBoxSprite2/ButtonSprite2Right")
@onready var button_sprite_3_left = grid_right.get_node("VBox3/PanelSprite3/HBoxSprite3/ButtonSprite3Left")
@onready var button_sprite_3_right = grid_right.get_node("VBox3/PanelSprite3/HBoxSprite3/ButtonSprite3Right")
@onready var button_sprite_4_left = grid_right.get_node("VBox4/PanelSprite4/HBoxSprite4/ButtonSprite4Left")
@onready var button_sprite_4_right = grid_right.get_node("VBox4/PanelSprite4/HBoxSprite4/ButtonSprite4Right")
@onready var sprite_1 = grid_right.get_node("VBox1/PanelSprite1/HBoxSprite1/TextureSprite1/Sprite1")
@onready var sprite_2 = grid_right.get_node("VBox2/PanelSprite2/HBoxSprite2/TextureSprite2/Sprite2")
@onready var sprite_3 = grid_right.get_node("VBox3/PanelSprite3/HBoxSprite3/TextureSprite3/Sprite3")
@onready var sprite_4 = grid_right.get_node("VBox4/PanelSprite4/HBoxSprite4/TextureSprite4/Sprite4")
@onready var check_box_1 = grid_right.get_node("VBox1/CheckBox1")
@onready var check_box_2 = grid_right.get_node("VBox2/CheckBox2")
@onready var check_box_3 = grid_right.get_node("VBox3/CheckBox3")
@onready var check_box_4 = grid_right.get_node("VBox4/CheckBox4")

@onready var message_dialog = $MessageDialog
@onready var vbox_dialog = $MessageDialog/MarginDialog/TextureDialog/MarginContent/VBoxContent
@onready var label_dialog = vbox_dialog.get_node("LabelDialog")
@onready var file_dialog = null


func _on_ready():
    build_file_dialog()
    setup_multiplayer()
    setup_right_panel()
    if global.music and music_player.current_track != music_player.main_theme:
        music_player.stop()
        music_player.current_track = music_player.main_theme


func _process(_delta):
    global.peer.poll()
    var state = global.peer.get_ready_state()
    if state == WebSocketPeer.STATE_OPEN:
        while global.peer.get_available_packet_count():
            packet_received(global.peer.get_packet())
    
    if OS.get_name() == "Android":
        if DisplayServer.virtual_keyboard_get_height() and (line_3.has_focus() or line_4.has_focus()):
            vbox_setup.position.y = DisplayServer.virtual_keyboard_get_height() * -1
        else:
            vbox_setup.position.y = 0


func _on_button_start_pressed():
    var error_text = get_tasks()
    if error_text:
        show_error_dialog(error_text)
        return
    
    set_team_frames_and_sounds()
    send_packet(global.clients, "game_started", "true")
    scene_transition.change_scene("res://scenes/game_online.tscn")


func _on_button_back_pressed():
    global.peer.close()
    scene_transition.change_scene("res://scenes/main.tscn")


func _on_button_file_pressed():
    if OS.get_name() in ["Linux", "macOS", "Windows"]:
        var current_dir = ""
        if FileAccess.file_exists(global.last_file):
            current_dir = global.last_file.get_base_dir()
        elif global.default_dir and DirAccess.dir_exists_absolute(global.default_dir):
            current_dir = global.default_dir
        var title = tr("Select a File")
        var mode = DisplayServer.FILE_DIALOG_MODE_OPEN_FILE
        DisplayServer.file_dialog_show(title, current_dir, "", false, mode, json_filter,
            _on_native_file_dialog_file_selected)
    elif OS.get_name() == "Web":
        JavaScriptBridge.eval("openFile()")
    else:
        if FileAccess.file_exists(global.last_file):
            file_dialog.current_dir = global.last_file.get_base_dir()
        elif global.default_dir and DirAccess.dir_exists_absolute(global.default_dir):
            file_dialog.current_dir = global.default_dir
        file_dialog.popup_centered(Vector2i(1600, 900))


func _on_tree_units_item_edited():
    check_player_ready(global.team1.name, check_box_1, sprite_1.sprite_frames) 
    var item = tree_units.get_selected()
    var index = item.get_index()
    var boolean = "true" if item.is_checked(0) else "false"
    var item_string = str(index) + "\t" + boolean
    send_packet(global.clients, "tree_unit_item", item_string)


func _on_button_distance_pressed(distance):
    global.game_distance = distance
    send_packet(global.clients, "button_distance", distance)


func _on_button_timer_pressed(duration):
    global.online_timer = duration
    send_packet(global.clients, "button_timer", duration)


func _on_button_background_left_pressed():
    if current_background == "random":
        return change_background("desert", global.background_desert, "")
    if current_background == "desert":
        return change_background("grassland", global.background_grassland, "")
    if current_background == "grassland":
        return change_background("forest", global.background_forest, "")
    if current_background == "forest":
        return change_background("random", global.background_random, "?")


func _on_button_background_right_pressed():
    if current_background == "random":
        return change_background("forest", global.background_forest, "")
    if current_background == "forest":
        return change_background("grassland", global.background_grassland, "")
    if current_background == "grassland":
        return change_background("desert", global.background_desert, "")
    if current_background == "desert":
        return change_background("random", global.background_random, "?")


func _on_line_1_text_changed(new_name):
    global.team1.name = new_name
    check_player_ready(global.team1.name, check_box_1, sprite_1.sprite_frames)
    send_packet(global.clients, "line_1", new_name)


func _on_line_2_text_changed(new_name):
    global.team2.name = new_name
    check_player_ready(global.team2.name, check_box_2, sprite_2.sprite_frames)
    send_packet(global.clients, "line_2", new_name)


func _on_line_3_text_changed(new_name):
    global.team3.name = new_name
    check_player_ready(global.team3.name, check_box_3, sprite_3.sprite_frames)
    send_packet(global.clients, "line_3", new_name)


func _on_line_4_text_changed(new_name):
    global.team4.name = new_name
    check_player_ready(global.team4.name, check_box_4, sprite_4.sprite_frames)
    send_packet(global.clients, "line_4", new_name)


func _on_line_focus_exited():
    if OS.get_name() == "Android":
        vbox_setup.position.y = 0


func _on_button_sprite_left_pressed(team_number):
    if team_number == 1:
        get_previous_sprite(global.team1, check_box_1, sprite_1)
    if team_number == 2:
        get_previous_sprite(global.team2, check_box_2, sprite_2)
    if team_number == 3:
        get_previous_sprite(global.team3, check_box_3, sprite_3)
    if team_number == 4:
        get_previous_sprite(global.team3, check_box_4, sprite_4)


func _on_button_sprite_right_pressed(team_number):
    if team_number == 1:
        get_next_sprite(global.team1, check_box_1, sprite_1)
    if team_number == 2:
        get_next_sprite(global.team2, check_box_2, sprite_2)
    if team_number == 3:
        get_next_sprite(global.team3, check_box_3, sprite_3)
    if team_number == 4:
        get_next_sprite(global.team3, check_box_4, sprite_4)


func _on_check_box_1_toggled(button_pressed):
    if not check_box_1.disabled and not button_pressed:
        change_label_network(message_ready, false)
    elif button_pressed:
        var error_text = check_tasks()
        if error_text:
            enable_button_start(error_text, true)
        else:
            var message = check_all_players_ready()
            enable_button_start(message, false)
    
    var boolean = "true" if button_pressed else "false"
    send_packet(global.clients, "check_box_1", boolean)


func _on_check_box_2_toggled(button_pressed):
    if not check_box_2.disabled and not button_pressed:
        change_label_network(message_ready, false)
    elif button_pressed:
        var message = check_all_players_ready()
        enable_button_start(message, false)
    
    var boolean = "true" if button_pressed else "false"
    send_packet(global.clients, "check_box_2", boolean)


func _on_check_box_3_toggled(button_pressed):    
    if not check_box_3.disabled and not button_pressed:
        change_label_network(message_ready, false)
    elif button_pressed:
        var message = check_all_players_ready()
        enable_button_start(message, false)
    
    var boolean = "true" if button_pressed else "false"
    send_packet(global.clients, "check_box_3", boolean)


func _on_check_box_4_toggled(button_pressed):
    if not check_box_4.disabled and not button_pressed:
        change_label_network(message_ready, false)
    elif button_pressed:
        var message = check_all_players_ready()
        enable_button_start(message, false)
    
    var boolean = "true" if button_pressed else "false"
    send_packet(global.clients, "check_box_4", boolean)


func _on_button_dialog_pressed():
    message_dialog.hide()
    if label_dialog.text == tr("The lobby was closed by the host."):
        global.clients.erase(global.client_index)
        global.peer.close()
        scene_transition.change_scene("res://scenes/main.tscn")


func _on_native_file_dialog_file_selected(_status, selected_paths, _selected_filter_index):
    if selected_paths.is_empty():
        return
    var file = FileAccess.open(selected_paths[0], FileAccess.READ)
    load_json_data([selected_paths[0], file.get_as_text()])


func _on_file_dialog_file_selected(path):
    path = path[0] if typeof(path) == TYPE_PACKED_STRING_ARRAY else path
    var file = FileAccess.open(path, FileAccess.READ)
    load_json_data([path, file.get_as_text()])


func build_file_dialog():
    if OS.get_name() in ["Linux", "macOS", "Windows"]:
        return
    elif OS.get_name() == "Web":
        java_script_callback = JavaScriptBridge.create_callback(load_json_data)
        JavaScriptBridge.get_interface("web_file_dialog").dataLoaded = java_script_callback
        return
    file_dialog = load("res://scenes/file_dialog.tscn").instantiate()
    file_dialog.cancel_button_text = tr("Cancel")
    file_dialog.ok_button_text = tr("Open")
    file_dialog.access = 2
    file_dialog.file_mode = 0
    file_dialog.set_filters(json_filter)
    file_dialog.file_selected.connect(_on_file_dialog_file_selected)
    file_dialog.files_selected.connect(_on_file_dialog_file_selected)
    add_child(file_dialog)


func setup_multiplayer():
    if global.host:
        setup_left_panel()
    else:
        if 0 in global.clients:
            send_packet([0], "get_host_data", [global.client_index])
        else:
            label_dialog.text = tr("The lobby was closed by the host.")
            message_dialog.show()


func setup_left_panel():    
    var color = Color(1.0, 1.0, 1.0, 1.0)
    button_start.show()
    button_file.disabled = false
    button_file.get_child(0).modulate = color
    label_file.add_theme_color_override("font_color", color)
    tree_units.modulate = color
    tree_units.mouse_filter = MOUSE_FILTER_STOP
    label_distance.add_theme_color_override("font_color", color)
    for node in hbox_distance.get_children():
        node.disabled = false
        node.get_child(0).add_theme_color_override("font_color", color)
    label_timer.add_theme_color_override("font_color", color)
    for node in hbox_timer.get_children():
        node.disabled = false
        node.get_child(0).add_theme_color_override("font_color", color)
    label_background.add_theme_color_override("font_color", color)
    panel_background.modulate = color
    button_background_left.disabled = false
    button_background_right.disabled = false
    
    if FileAccess.file_exists(global.last_file):
        _on_file_dialog_file_selected(global.last_file)
    if global.game_distance == 1:
        button_distance_short.set_pressed(true)
    if global.game_distance == 3:
        button_distance_long.set_pressed(true)
    if global.online_timer == 1:
        button_timer_short.set_pressed(true)
    if global.online_timer == 3:
        button_timer_long.set_pressed(true)
    if global.background == "desert":
        change_background("desert", global.background_desert, "")
    if global.background == "forest":
        change_background("forest", global.background_forest, "")
    if global.background == "grassland":
        change_background("grassland", global.background_grassland, "")


func setup_right_panel():   
    if global.client_index == 0:
        line_1.text = global.team1.name
        modify_nodes(check_box_1, check_button_1, label_1, line_1, panel_sprite_1,
                button_sprite_1_left, button_sprite_1_right, global.team1.name, true, 1.0)
        show_sprite(global.team1, check_box_1, sprite_1)
        check_player_ready(global.team1.name, check_box_1, sprite_1.sprite_frames)
        if len(global.clients) > 1:
            host_returned()
    if global.client_index == 1:
        line_2.text = global.team2.name
        modify_nodes(check_box_2, check_button_2, label_2, line_2, panel_sprite_2,
                button_sprite_2_left, button_sprite_2_right, global.team2.name, true, 1.0)
        show_sprite(global.team2, check_box_2, sprite_2)
        check_player_ready(global.team2.name, check_box_2, sprite_2.sprite_frames)
        send_packet(global.clients, "check_button_2", "true")
    if global.client_index == 2:
        line_3.text = global.team3.name
        modify_nodes(check_box_3, check_button_3, label_3, line_3, panel_sprite_3,
                button_sprite_3_left, button_sprite_3_right, global.team3.name, true, 1.0)
        show_sprite(global.team3, check_box_3, sprite_3)
        check_player_ready(global.team3.name, check_box_3, sprite_3.sprite_frames)
        send_packet(global.clients, "check_button_3", "true")
    if global.client_index == 3:
        line_4.text = global.team4.name
        modify_nodes(check_box_4, check_button_4, label_4, line_4, panel_sprite_4,
                button_sprite_4_left, button_sprite_4_right, global.team4.name, true, 1.0)
        show_sprite(global.team4, check_box_4, sprite_4)
        check_player_ready(global.team4.name, check_box_4, sprite_4.sprite_frames)
        send_packet(global.clients, "check_button_4", "true")


func load_json_data(data):
    var path = data[0]
    var string = data[1]
    var json = JSON.new()
    var error = json.parse(string)
    tree_units.clear()
    
    if error != OK:
        label_file.text = tr("No File Loaded")
        show_error_dialog(tr("ERROR:\nThe file could not be opened!"))
        return
    
    tasks_dict = json.get_data()
    var units = tasks_dict.keys()
    if not check_json_data_structure(units):
        label_file.text = tr("No File Loaded")
        show_error_dialog(tr("ERROR:\nThe file data is incomplete!"))
        return
    
    var root = tree_units.create_item()
    for unit in units:
        var new_unit = tree_units.create_item(root)
        new_unit.set_cell_mode(0, 1)
        new_unit.set_editable(0, true)
        new_unit.set_text(0, unit)
    
    global.last_file = path
    label_file.text = path.get_file()
    check_player_ready(global.team1.name, check_box_1, sprite_1.sprite_frames)
    
    send_packet(global.clients, "label_file", label_file.text)
    send_packet(global.clients, "tree_units", "\t".join(units))


func check_json_data_structure(units):
    for unit in units:
        for task in tasks_dict[unit]:
            if not "id" in task or not "category" in task or not "difficulty" in task \
                    or not "question" in task or not "answer" in task:
                return false
    return true


func change_background(bg_name, bg_resource, bg_text):
    global.background = bg_name
    current_background = bg_name
    background_preview.texture = bg_resource
    background_window.texture = bg_resource
    background_label.text = bg_text
    
    send_packet(global.clients, "background", bg_name)


func modify_nodes(check_box, check_button, label, line, panel_sprite, button_sprite_left,
        button_sprite_right, player_name, boolean, alpha):
    check_button.button_pressed = boolean
    check_button.add_theme_color_override("font_disabled_color", Color(1.0, 1.0, 1.0, alpha))
    label.add_theme_color_override("font_color", Color(1.0, 1.0, 1.0, alpha))
    line.editable = boolean
    panel_sprite.modulate = Color(1.0, 1.0, 1.0, alpha)
    button_sprite_left.disabled = not boolean
    button_sprite_right.disabled = not boolean
    if boolean:
        line.text = player_name
        line.mouse_filter = MOUSE_FILTER_STOP
        check_box.disabled = false if player_name else true
        check_box.mouse_filter = MOUSE_FILTER_STOP
    else:
        line.text = ""
        line.mouse_filter = MOUSE_FILTER_IGNORE
        line.release_focus()
        check_box.button_pressed = false
        check_box.mouse_filter = MOUSE_FILTER_IGNORE


func show_sprite(team, check_box, sprite):
    sprite.sprite_frames = team.frames if team.frames else sprite_frames[team.number - 1]
    sprite.get_parent().texture = null
    sprite.play("idle")
    sprite.show()
    
    check_player_ready(team.name, check_box, sprite.sprite_frames)
    var sprite_number = "sprite_" + str(team.number)
    send_packet(global.clients, sprite_number, sprite.sprite_frames.resource_path)


func hide_sprite(sprite):
    sprite.sprite_frames = null
    sprite.get_parent().texture = load("res://tres/texture_sprite_empty.tres")
    sprite.hide()


func get_previous_sprite(team, check_box, sprite):
    var index = sprite_frames.find(sprite.sprite_frames)
    if index == 0:
        index = sprite_frames.size() - 1
    else:
        index -= 1
    sprite.sprite_frames = sprite_frames[index]
    sprite.play("idle")
    
    check_player_ready(team.name, check_box, sprite.sprite_frames)
    var sprite_number = "sprite_" + str(team.number)
    send_packet(global.clients, sprite_number, sprite.sprite_frames.resource_path)


func get_next_sprite(team, check_box, sprite):
    var index = sprite_frames.find(sprite.sprite_frames)
    if index == sprite_frames.size() - 1:
        index = 0
    else:
        index += 1
    sprite.sprite_frames = sprite_frames[index]
    sprite.play("idle")
    
    check_player_ready(team.name, check_box, sprite.sprite_frames)
    var sprite_number = "sprite_" + str(team.number)
    send_packet(global.clients, sprite_number, sprite.sprite_frames.resource_path)


func get_tasks():
    global.tasks = []
    global.tasks_easy = []
    global.tasks_medium = []
    global.tasks_hard = []
    
    var units_selected = []
    for unit in tree_units.get_root().get_children():
        if unit.is_checked(0):
            units_selected.append(unit.get_text(0))
            for task in tasks_dict[unit.get_text(0)]:
                global.tasks.append(task)

    for task in global.tasks:
        if task.difficulty == 1:
            global.tasks_easy.append(task)
        if task.difficulty == 2:
            global.tasks_medium.append(task)
        if task.difficulty == 3:
            global.tasks_hard.append(task)
    if not global.tasks_easy or not global.tasks_medium or not global.tasks_hard:
        return tr("Please select units including all levels of difficulty!")
    return ""


func check_player_ready(player_name, check_box, player_sprite_frames):
    var error_text = ""
    error_text = check_player_data(player_name, player_sprite_frames)
    if error_text:
        change_label_network(error_text, true)
        check_box.disabled = true
        check_box.button_pressed = false
    else:
        check_box.disabled = false
        if not check_box.button_pressed:
            return change_label_network(message_ready, false)
        if global.host:
            error_text = check_tasks()
            if error_text:
                enable_button_start(error_text, true)
            else:
                var message = check_all_players_ready()
                enable_button_start(message, false)


func check_player_data(player_name, player_sprite_frames):
    if player_name == "":
        return message_no_name
    
    var player_names = []
    var player_frames = []
    if global.client_index != 0 and check_button_1.button_pressed:
        if line_1.text:
            player_names.append(line_1.text)
        player_frames.append(sprite_1.sprite_frames)
    if global.client_index != 1 and check_button_2.button_pressed:
        if line_2.text:
            player_names.append(line_2.text)
        player_frames.append(sprite_2.sprite_frames)
    if global.client_index != 2 and check_button_3.button_pressed:
        if line_3.text:
            player_names.append(line_3.text)
        player_frames.append(sprite_3.sprite_frames)
    if global.client_index != 3 and check_button_4.button_pressed:
        if line_4.text:
            player_names.append(line_4.text)
        player_frames.append(sprite_4.sprite_frames)
    if player_name in player_names:
        return message_same_name
    if player_sprite_frames in player_frames:
        return message_same_frame
    
    return ""


func check_tasks():
    if not tree_units.get_root():
        return tr("Please open a file with tasks!")
    
    var unit_selected = false
    for unit in tree_units.get_root().get_children():
        if unit.is_checked(0):
            unit_selected = true
            break
    if not unit_selected:
        return tr("Please select at least one unit!")
    
    return ""


func check_all_players_ready():
    var player_number = [check_button_1.button_pressed, check_button_2.button_pressed,
            check_button_3.button_pressed, check_button_4.button_pressed]
    if player_number.count(true) == 1:
        return message_no_players
    if check_button_1.button_pressed:
        if not check_box_1.button_pressed:
            return message_ready_all
    if check_button_2.button_pressed:
        if not check_box_2.button_pressed:
            return message_ready_all
    if check_button_3.button_pressed:
        if not check_box_3.button_pressed:
            return message_ready_all
    if check_button_4.button_pressed:
        if not check_box_4.button_pressed:
            return message_ready_all
    return ""


func change_label_network(text, warning):
    label_network.text = text
    if warning:
        icon_warning.show()
    else:
        icon_warning.hide()


func enable_button_start(message, warning):
    if message:
        button_start.disabled = true
        button_start.get_child(0).modulate = Color(1, 1, 1, 0.5)
        change_label_network(message, warning)
    else:
        button_start.disabled = false
        button_start.get_child(0).modulate = Color(1, 1, 1, 1)
        if global.host:
            change_label_network(message_start_server, warning)
        else:
            change_label_network(message_start_client, warning)


func show_error_dialog(error_text):
    if file_dialog:
        file_dialog.hide()
    label_dialog.text = error_text
    message_dialog.show()


func set_team_frames_and_sounds():
    global.team1.frames = sprite_1.sprite_frames
    global.team2.frames = sprite_2.sprite_frames
    global.team3.frames = sprite_3.sprite_frames
    global.team4.frames = sprite_4.sprite_frames

    for team in [global.team1, global.team2, global.team3, global.team4]:
        if "adventure" in team.frames.resource_path:
            team.sound = sound_running_adventure_boy
        elif "cat" in team.frames.resource_path:
            team.sound = sound_running_cat
        elif "dog" in team.frames.resource_path:
            team.sound = sound_running_dog
        elif "knight" in team.frames.resource_path:
            team.sound = sound_running_knight
        elif "ninja" in team.frames.resource_path:
            team.sound = sound_running_ninja_girl
        elif "robot" in team.frames.resource_path:
            team.sound = sound_running_robot
        elif "zombie_boy" in team.frames.resource_path:
            team.sound = sound_running_zombie_boy
        elif "zombie_girl" in team.frames.resource_path:
            team.sound = sound_running_zombie_girl


func send_packet(receivers, topic, data):
    if global.host and receivers == [0]:
        return
    var packet = {"receivers": receivers, "topic": topic, "data": data}
    global.peer.send_text(JSON.stringify(packet, "  "))


func packet_received(packet):
    var json = JSON.new()
    var error = json.parse(packet.get_string_from_utf8())
    if error != OK:
        return
    
    var topic = json.data["topic"]
    var data = json.data["data"]
    
    if topic == "client_connected":
        global.clients.append(int(data[0]))
        if label_network.text == message_start_server or label_network.text == message_start_client:
            enable_button_start(message_ready_all, false)
    
    if topic == "client_disconnected":
        var client_index = int(data[0])
        global.clients.erase(client_index)
        if client_index == 0:
            label_dialog.text = tr("The lobby was closed by the host.")
            message_dialog.show()
            return
        if client_index == 1:
            global.team2.name = ""
            modify_nodes(check_box_2, check_button_2, label_2, line_2, panel_sprite_2,
                    button_sprite_2_left, button_sprite_2_right, "", false, 0.4)
            hide_sprite(sprite_2)
        if client_index == 2:
            global.team3.name = ""
            modify_nodes(check_box_3, check_button_3, label_3, line_3, panel_sprite_3,
                    button_sprite_3_left, button_sprite_3_right, "", false, 0.4)
            hide_sprite(sprite_3)
        if client_index == 3:
            global.team4.name = ""
            modify_nodes(check_box_4, check_button_4, label_4, line_4, panel_sprite_4,
                    button_sprite_4_left, button_sprite_4_right, "", false, 0.4)
            hide_sprite(sprite_4)
        if not icon_warning.visible:
            var message = check_all_players_ready()
            enable_button_start(message, false)
        return
    
    if topic == "get_host_data":
        if label_network.text == message_no_players:
            label_network.text = message_ready_all
        
        send_packet(data, "label_file", label_file.text)
        send_packet(data, "button_distance", global.game_distance)
        send_packet(data, "button_timer", global.online_timer)
        send_packet(data, "background", global.background)
        
        if tree_units.get_root():
            var units = []
            var unit_items = tree_units.get_root().get_children()
            for unit_item in unit_items:
                units.append(unit_item.get_text(0))
            var units_string = "\t".join(units)
            send_packet(data, "tree_units", units_string)
            
            for unit_item in unit_items:
                if unit_item.is_checked(0):
                    var item_string = str(unit_item.get_index()) + "\t" + "true"
                    send_packet(data, "tree_unit_item", item_string)
        
        for check_button in [check_button_1, check_button_2, check_button_3, check_button_4]:
            if check_button.button_pressed:
                send_packet(data, check_button.name.to_snake_case(), "true")
        
        for line in [line_1, line_2, line_3, line_4]:
            if line.text:
                send_packet(data, line.name.to_snake_case(), line.text)
        
        for sprite in [sprite_1, sprite_2, sprite_3, sprite_4]:
            if sprite.get_parent().texture == null:
                var sprite_path = sprite.sprite_frames.resource_path
                send_packet(data, sprite.name.to_snake_case(), sprite_path)
        
        for check_ready in [check_box_1, check_box_2, check_box_3, check_box_4]:
            if check_ready.button_pressed:
                send_packet(data, check_ready.name.to_snake_case(), "true")
        
        return
    
    if topic == "host_returned":
        label_network.text = message_ready_all
        if global.client_index == 1:
            send_packet(global.clients, "check_button_2", "true")
            send_packet(global.clients, "line_2", line_2.text)
            send_packet(global.clients, "sprite_2", sprite_2.sprite_frames.resource_path)
            var boolean_2 = "true" if check_box_2.button_pressed else "false"
            send_packet(global.clients, "check_box_2", boolean_2)
        if global.client_index == 2:
            send_packet(global.clients, "check_button_3", "true")
            send_packet(global.clients, "line_3", line_3.text)
            send_packet(global.clients, "sprite_3", sprite_3.sprite_frames.resource_path)
            var boolean_3 = "true" if check_box_3.button_pressed else "false"
            send_packet(global.clients, "check_box_3", boolean_3)
        if global.client_index == 3:
            send_packet(global.clients, "check_button_4", "true")
            send_packet(global.clients, "line_4", line_4.text)
            send_packet(global.clients, "sprite_4", sprite_4.sprite_frames.resource_path)
            var boolean_4 = "true" if check_box_4.button_pressed else "false"
            send_packet(global.clients, "check_box_4", boolean_4)
        return
    
    if topic == "label_file":
        label_file.text = data
        return
    
    if topic == "tree_units":
        tree_units.clear()
        var root = tree_units.create_item()
        for unit in data.split("\t"):
            var new_unit = tree_units.create_item(root)
            new_unit.set_cell_mode(0, 1)
            new_unit.set_text(0, unit)
        return
    
    if topic == "tree_unit_item":
        var item_data = data.split("\t")
        var tree_item = tree_units.get_root().get_child(int(item_data[0]))
        for child in tree_units.get_root().get_children():
            if child == tree_item:
                child.set_checked(0, data.contains("true"))
                break
        return
    
    if topic == "button_distance":
        if int(data) == 1:
            global.game_distance = 1
            button_distance_short.button_pressed = true
            button_distance_short.texture_disabled = button_distance_short.texture_pressed
            button_distance_normal.texture_disabled = null
            button_distance_long.texture_disabled = null
        if int(data) == 2:
            global.game_distance = 2
            button_distance_normal.button_pressed = true
            button_distance_short.texture_disabled = null
            button_distance_normal.texture_disabled = button_distance_normal.texture_pressed
            button_distance_long.texture_disabled = null
        if int(data) == 3:
            global.game_distance = 3
            button_distance_long.button_pressed = true
            button_distance_short.texture_disabled = null
            button_distance_normal.texture_disabled = null
            button_distance_long.texture_disabled = button_distance_long.texture_pressed
        return
    
    if topic == "button_timer":
        if int(data) == 1:
            global.online_timer = 1
            button_timer_short.button_pressed = true
            button_timer_short.texture_disabled = button_timer_short.texture_pressed
            button_timer_normal.texture_disabled = null
            button_timer_long.texture_disabled = null
        if int(data) == 2:
            global.online_timer = 2
            button_timer_normal.button_pressed = true
            button_timer_short.texture_disabled = null
            button_timer_normal.texture_disabled = button_timer_normal.texture_pressed
            button_timer_long.texture_disabled = null
        if int(data) == 3:
            global.online_timer = 3
            button_timer_long.button_pressed = true
            button_timer_short.texture_disabled = null
            button_timer_normal.texture_disabled = null
            button_timer_long.texture_disabled = button_timer_long.texture_pressed
        return
    
    if topic == "background":
        current_background = data
        global.background = data
        if current_background == "desert":
            background_preview.texture = global.background_desert
            background_window.texture = global.background_desert
            background_label.text = ""
        if current_background == "grassland":
            background_preview.texture = global.background_grassland
            background_window.texture = global.background_grassland
            background_label.text = ""
        if current_background == "forest":
            background_preview.texture = global.background_forest
            background_window.texture = global.background_forest
            background_label.text = ""
        if current_background == "random":
            background_preview.texture = global.background_random
            background_window.texture = global.background_random
            background_label.text = "?"
        return
    
    if topic == "check_button_1":
        check_button_1.button_pressed = data.contains("true")
        if data.contains("true"):
            line_1.text = global.team1.name
        else:
            global.team1.name = ""
            modify_nodes(check_box_1, check_button_1, label_1, line_1, panel_sprite_1,
                    button_sprite_1_left, button_sprite_1_right, "", false, 0.4)
            hide_sprite(sprite_1)
        return
    
    if topic == "check_button_2":
        check_button_2.button_pressed = data.contains("true")
        if data.contains("true"):
            line_2.text = global.team2.name
        else:
            global.team2.name = ""
            modify_nodes(check_box_2, check_button_2, label_2, line_2, panel_sprite_2,
                    button_sprite_2_left, button_sprite_2_right, "", false, 0.4)
            hide_sprite(sprite_2)
        return
    
    if topic == "check_button_3":
        check_button_3.button_pressed = data.contains("true")
        if data.contains("true"):
            line_3.text = global.team3.name
        else:
            global.team3.name = ""
            modify_nodes(check_box_3, check_button_3, label_3, line_3, panel_sprite_3,
                    button_sprite_3_left, button_sprite_3_right, "", false, 0.4)
            hide_sprite(sprite_3)
        return
    
    if topic == "check_button_4":
        check_button_4.button_pressed = data.contains("true")
        if data.contains("true"):
            line_4.text = global.team4.name
        else:
            global.team4.name = ""
            modify_nodes(check_box_4, check_button_4, label_4, line_4, panel_sprite_4,
                    button_sprite_4_left, button_sprite_4_right, "", false, 0.4)
            hide_sprite(sprite_3)
        return
    
    if topic == "line_1":
        global.team1.name = data
        line_1.text = data
        check_double_names_or_frames()
        return
    
    if topic == "line_2":
        global.team2.name = data
        line_2.text = data
        check_double_names_or_frames()
        return
    
    if topic == "line_3":
        global.team3.name = data
        line_3.text = data
        check_double_names_or_frames()
        return
    
    if topic == "line_4":
        global.team4.name = data
        line_4.text = data
        check_double_names_or_frames()
        return
    
    if topic == "sprite_1":
        sprite_1.get_parent().texture = null
        sprite_1.sprite_frames = load(data)
        sprite_1.play("idle")
        sprite_1.show()
        check_double_names_or_frames()
        return
    
    if topic == "sprite_2":
        sprite_2.get_parent().texture = null
        sprite_2.sprite_frames = load(data)
        sprite_2.play("idle")
        sprite_2.show()
        check_double_names_or_frames()
        return
    
    if topic == "sprite_3":
        sprite_3.get_parent().texture = null
        sprite_3.sprite_frames = load(data)
        sprite_3.play("idle")
        sprite_3.show()
        check_double_names_or_frames()
        return
    
    if topic == "sprite_4":
        sprite_4.get_parent().texture = null
        sprite_4.sprite_frames = load(data)
        sprite_4.play("idle")
        sprite_4.show()
        check_double_names_or_frames()
        return

    if topic == "check_box_1":
        check_box_1.set_pressed_no_signal(data.contains("true"))
        if icon_warning.visible or label_network.text == message_ready:
            return
        else:
            var message = check_all_players_ready()
            return enable_button_start(message, false)

    if topic == "check_box_2":
        check_box_2.set_pressed_no_signal(data.contains("true"))
        if icon_warning.visible or label_network.text == message_ready:
            return
        else:
            var message = check_all_players_ready()
            return enable_button_start(message, false)

    if topic == "check_box_3":
        check_box_3.set_pressed_no_signal(data.contains("true"))
        if icon_warning.visible or label_network.text == message_ready:
            return
        else:
            var message = check_all_players_ready()
            return enable_button_start(message, false)

    if topic == "check_box_4":
        check_box_4.set_pressed_no_signal(data.contains("true"))
        if icon_warning.visible or label_network.text == message_ready:
            return
        else:
            var message = check_all_players_ready()
            return enable_button_start(message, false)
    
    if topic == "game_started" and data == "true":
        set_team_frames_and_sounds()
        scene_transition.change_scene("res://scenes/game_online.tscn")
        return


func check_double_names_or_frames():
    if global.client_index == 0:
        check_player_ready(global.team1.name, check_box_1, sprite_1.sprite_frames)
    elif global.client_index == 1:
        check_player_ready(global.team2.name, check_box_2, sprite_2.sprite_frames)
    elif global.client_index == 2:
        check_player_ready(global.team3.name, check_box_3, sprite_3.sprite_frames)
    elif global.client_index == 3:
        check_player_ready(global.team4.name, check_box_4, sprite_4.sprite_frames)


func host_returned():
    label_network.text = message_ready_all
    send_packet(global.clients, "button_distance", global.game_distance)
    send_packet(global.clients, "button_timer", global.online_timer)
    send_packet(global.clients, "background", global.background)
    send_packet(global.clients, "check_button_1", "true")
    send_packet(global.clients, "line_1", line_1.text)
    send_packet(global.clients, "sprite_1", sprite_1.sprite_frames.resource_path)
    send_packet(global.clients, "host_returned", "")
