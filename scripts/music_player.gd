extends Control


var current_track = {}

var background_music = [
    {"path": load("res://assets/music/background_01.mp3"), "db": -15, "loop": false},
    {"path": load("res://assets/music/background_02.mp3"), "db": -17, "loop": false},
    {"path": load("res://assets/music/background_03.mp3"), "db": -14, "loop": false},
    {"path": load("res://assets/music/background_04.mp3"), "db": -14, "loop": false},
    {"path": load("res://assets/music/background_05.mp3"), "db": -18, "loop": false},
    {"path": load("res://assets/music/background_06.mp3"), "db": -13, "loop": false},
    {"path": load("res://assets/music/background_07.mp3"), "db": -16, "loop": false},
    {"path": load("res://assets/music/background_08.mp3"), "db": -10, "loop": false},
    {"path": load("res://assets/music/background_09.mp3"), "db": -13, "loop": false},
    {"path": load("res://assets/music/background_10.mp3"), "db": -15, "loop": false},
    {"path": load("res://assets/music/background_11.mp3"), "db": -12, "loop": false},
    {"path": load("res://assets/music/background_12.mp3"), "db": -8, "loop": false},
    {"path": load("res://assets/music/background_13.mp3"), "db": -11, "loop": false},
    {"path": load("res://assets/music/background_14.mp3"), "db": -7, "loop": false}]
var main_theme = {"path": load("res://assets/music/main_theme.mp3"), "db": -16, "loop": true}
var main_theme_xmas = {"path": load("res://assets/music/main_theme_xmas.mp3"), "db": -8, "loop": true}


@onready var animation = $AnimationMusicPlayer
@onready var audio_stream = $AudioStreamMusicPlayer


func _init():
    if Time.get_date_dict_from_system()["month"] == 12:
        main_theme = main_theme_xmas


func _on_audio_stream_finished():
    var index = background_music.find(current_track) + 1
    if index == len(background_music):
        index = 0
    play(background_music[index])


func _on_animation_finished(_animation_name):
    audio_stream.stop()
    if current_track:
        play(current_track)


func play(track):
    current_track = track
    audio_stream.stream = track["path"]
    audio_stream.stream.loop = track["loop"]
    audio_stream.volume_db = track["db"]
    audio_stream.play()


func stop():
    animation.get_animation("fade_out").track_set_key_value(0, 0, audio_stream.volume_db)
    animation.play("fade_out")
    current_track = {}
