class_name Setup
extends Control


const url = "wss://romaaeterna.fly.dev/"
# const url = "ws://localhost:9080"         # for local testing

var connected = false
var init_lobby = false
var lobby_name = ""

@onready var vbox_setup =  $VBoxSetup
@onready var hbox_main =  vbox_setup.get_node("TextureBackground/MarginMain/HBoxMain")
@onready var vbox_right = hbox_main.get_node("PanelRight/MarginRight/VBoxRight")
@onready var line_lobby = vbox_right.get_node("HBoxLobby/LineLobby")

@onready var dialog_error = $DialogError
@onready var vbox_error = $DialogError/MarginError/TextureError/MarginContent/VBoxContent
@onready var label_error = vbox_error.get_node("LabelError")

@onready var dialog_connect = $DialogConnect
@onready var timer_connect = $DialogConnect/TimerConnect
@onready var vbox_connect = $DialogConnect/MarginConnect/TextureConnect/MarginContent/VBoxContent
@onready var sprite_connect = vbox_connect.get_node("CenterConnect/SpriteConnect")


func _on_ready():
    global.peer = WebSocketPeer.new()


func _process(_delta):
    if connected:
        global.peer.poll()
        var state = global.peer.get_ready_state()
        if state == WebSocketPeer.STATE_OPEN:
            if init_lobby:
                var packet = {"lobby_game": "properate", "lobby_host": global.host,
                    "lobby_name": lobby_name}
                global.peer.send_text(JSON.stringify(packet, "  "))
                init_lobby = false
            while global.peer.get_available_packet_count():
                packet_received(global.peer.get_packet())
        if state == WebSocketPeer.STATE_CLOSED:
            connected = false
    
    if OS.get_name() == "Android":
        if DisplayServer.virtual_keyboard_get_height():
            vbox_setup.position.y = DisplayServer.virtual_keyboard_get_height() * -1
        else:
            vbox_setup.position.y = 0


func _on_button_back_pressed():
    global.peer.close()
    scene_transition.change_scene("res://scenes/main.tscn")


func _on_button_team_mode_pressed():
    global.game_mode = "team"
    scene_transition.change_scene("res://scenes/setup_local.tscn")


func _on_button_player_mode_pressed():
    global.game_mode = "player"
    scene_transition.change_scene("res://scenes/setup_local.tscn")


func _on_line_lobby_focus_exited():
    if OS.get_name() == "Android":
        line_lobby.release_focus()
        vbox_setup.position.y = 0


func _on_line_lobby_text_submitted(_new_text):
    _on_button_join_server_pressed()


func _on_button_create_server_pressed():
    lobby_name = line_lobby.text.to_lower()
    if lobby_name == "":
        label_error.text = tr("Please type in the name of the lobby you want to create!")
        dialog_error.show()
        return
    
    dialog_connect.show()
    sprite_connect.play("spinning")
    timer_connect.start()
    
    global.peer.connect_to_url(url)
    global.host = true
    connected = true
    init_lobby = true


func _on_button_join_server_pressed():
    lobby_name = line_lobby.text.to_lower()
    if lobby_name == "":
        label_error.text = tr("Please type in the name of the lobby you want to join!")
        dialog_error.show()
        return
    
    dialog_connect.show()
    sprite_connect.play("spinning")
    timer_connect.start()
    
    global.peer.connect_to_url(url)
    global.host = false
    connected = true
    init_lobby = true


func _on_button_error_pressed():
    dialog_error.hide()


func _on_timer_connect_timeout():
    global.peer.close()
    dialog_connect.hide()
    label_error.text = tr("Cannot connect to server!")
    dialog_error.show()


func packet_received(packet):
    var json = JSON.new()
    var error = json.parse(packet.get_string_from_utf8())
    if error != OK:
        return
    
    var topic = json.data["topic"]
    var data = json.data["data"]
    
    if topic == "client_connected":
        global.client_index = int(data[0])
        global.clients = data[1].map(func(x): return int(x))
        scene_transition.change_scene("res://scenes/setup_online.tscn")
        dialog_connect.hide()
        timer_connect.stop()
        reset_teams()
        set_process(false)
        return

    if topic in ["lobby_exists", "lobby_full", "lobby_not_found", "lobby_max_reached",
        "game_already_started"]:
        global.peer.close()
        if topic == "lobby_exists":
            label_error.text = tr("The lobby you want to create already exists.")
        if topic == "lobby_full":
            label_error.text = tr("There are already four players in the lobby.")
        if topic == "lobby_not_found":
            label_error.text = tr("There is no lobby with the given name.")
        if topic == "lobby_max_reached":
            label_error.text = tr("The maximum number of lobbys is reached.")
        if topic == "game_already_started":
            label_error.text = tr("The game you want to join has already started.")
        dialog_connect.hide()
        timer_connect.stop()
        dialog_error.show()
        return


func reset_teams():
    global.team1.name = ""
    global.team2.name = ""
    global.team3.name = ""
    global.team4.name = ""
    global.team1.frames = null
    global.team2.frames = null
    global.team3.frames = null
    global.team4.frames = null
