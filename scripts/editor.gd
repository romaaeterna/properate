class_name Editor
extends Control


var categories = []
var current_file_path = ""
var current_task = null
var current_unit = null
var java_script_callback = null
var json_filter = PackedStringArray(["*.json ; JSON Files"])
var tasks_on_load = {}
var tasks_to_save = {}
var unit_names = []
var units = []
var warning_type = "close"


@onready var vbox_editor = $VBoxEditor
@onready var hbox_file = vbox_editor.get_node("MarginTop/HBoxFile")
@onready var button_save = hbox_file.get_node("ButtonSave")
@onready var animation_save = hbox_file.get_node("AnimationSave")
@onready var label_file = vbox_editor.get_node("MarginTop/CenterFile/LabelFile")
@onready var button_music = vbox_editor.get_node("MarginTop/HBoxRight/ButtonMusic")

@onready var hbox_main = vbox_editor.get_node("TextureBackground/MarginMain/HBoxMain")
@onready var vbox_left = hbox_main.get_node("PanelLeft/MarginLeft/VBoxLeft")
@onready var tree_units = vbox_left.get_node("VBoxUnits/TreeUnits")
@onready var hbox_units = vbox_left.get_node("VBoxUnits/HBoxUnits")
@onready var button_unit_add = hbox_units.get_node("ButtonUnitAdd")
@onready var button_unit_remove = hbox_units.get_node("ButtonUnitRemove")
@onready var tree_categories = vbox_left.get_node("VBoxCategories/TreeCategories")
@onready var hbox_categories = vbox_left.get_node("VBoxCategories/HBoxCategories")
@onready var button_category_add = hbox_categories.get_node("ButtonCategoryAdd")
@onready var button_category_remove = hbox_categories.get_node("ButtonCategoryRemove")

@onready var hbox_right = hbox_main.get_node("PanelRight/MarginRight/HBoxRight")
@onready var button_task_left = hbox_right.get_node("ButtonTaskLeft")
@onready var button_task_right = hbox_right.get_node("ButtonTaskRight")
@onready var vbox_task = hbox_right.get_node("PanelTask/MarginTask/VBoxTask")
@onready var grid_task = vbox_task.get_node("MarginTaskData/GridTask")
@onready var label_unit = grid_task.get_node("LabelUnitName")
@onready var button_category = grid_task.get_node("ButtonCategory")
@onready var button_easy = grid_task.get_node("HBoxDifficulty/ButtonEasy")
@onready var button_medium = grid_task.get_node("HBoxDifficulty/ButtonMedium")
@onready var button_hard = grid_task.get_node("HBoxDifficulty/ButtonHard")
@onready var line_question = grid_task.get_node("LineQuestion")
@onready var line_answer = grid_task.get_node("LineAnswer")
@onready var label_task_number = vbox_task.get_node("HBoxTask/LabelTaskNumber")
@onready var button_task_remove = vbox_task.get_node("HBoxTask/ButtonTaskRemove")
@onready var hbox_warning = vbox_task.get_node("HBoxWarning")
@onready var label_warning = hbox_warning.get_node("LabelWarning")

@onready var message_dialog = $MessageDialog
@onready var vbox_dialog = $MessageDialog/MarginDialog/TextureDialog/MarginContent/VBoxContent
@onready var label_dialog = vbox_dialog.get_node("LabelDialog")
@onready var button_dialog_back = vbox_dialog.get_node("VBoxButtons/ButtonDialogBack")
@onready var button_dialog_close = vbox_dialog.get_node("VBoxButtons/ButtonDialogClose")
@onready var file_dialog = null


func _on_ready():
    set_process(false)
    build_file_dialog()
    button_music.visible = true if global.music else false


func _process(_delta):
    if DisplayServer.virtual_keyboard_get_height():
        vbox_editor.position.y = DisplayServer.virtual_keyboard_get_height() * -1
    else:
        vbox_editor.position.y = 0


func _on_button_new_pressed():
    get_tasks_to_save()
    if tasks_on_load.hash() != tasks_to_save.hash():
        warning_type = "new_file"
        var warning_text = tr("WARNING:\nIf you create a new document all changes will be discarded!")
        var button_text = tr("Create without Saving")
        show_warning_dialog(warning_text, button_text)
        return
    clear_all_data()


func _on_button_open_pressed():
    get_tasks_to_save()
    if tasks_on_load.hash() != tasks_to_save.hash():
        warning_type = "open_file"
        var warning_text = tr("WARNING:\nIf you open a new document all changes will be discarded!")
        var button_text = tr("Open without Saving")
        show_warning_dialog(warning_text, button_text)
        return
    popup_open_file_dialog()


func _on_button_save_pressed():
    if current_file_path or OS.get_name() == "Web":
        get_tasks_to_save()
        save_json_data(current_file_path)
    else:
        if OS.get_name() in ["Linux", "macOS", "Windows"]:
            var title = tr("Save a File")
            var mode = DisplayServer.FILE_DIALOG_MODE_SAVE_FILE
            DisplayServer.file_dialog_show(title, global.default_dir, "", false, mode, json_filter,
                _on_native_file_dialog_save_file)
        else:
            file_dialog.cancel_button_text = tr("Cancel")
            file_dialog.ok_button_text = tr("Save")
            file_dialog.file_mode = 4
            file_dialog.current_dir = global.default_dir
            file_dialog.popup_centered(Vector2i(1600, 900))


func _on_button_music_toggled(toggle_mode):
    music_player.audio_stream.stream_paused = false if toggle_mode else true


func _on_button_back_pressed():
    get_tasks_to_save()
    if tasks_to_save and tasks_on_load.hash() != tasks_to_save.hash():
        warning_type = "close"
        var warning_text = tr("WARNING:\nIf you close the window all changes will be discarded!")
        var button_text = tr("Close without Saving")
        show_warning_dialog(warning_text, button_text)
        return
    if global.music:
        music_player.audio_stream.stream_paused = false
    scene_transition.change_scene("res://scenes/main.tscn")


func _on_button_unit_add_pressed():
    if not tree_units.get_root():
        tree_units.create_item()
    var unit_name = get_new_name(tr("New Unit"), unit_names)
    var unit_id = get_new_id(units)
    var unit_item = tree_units.create_item(tree_units.get_root())
    var unit_object = global.unit.new(unit_id, unit_item, unit_name)
    units.append(unit_object)
    unit_names.append(unit_name)
    unit_item.set_editable(0, true)
    unit_item.set_text(0, unit_name)
    
    unit_item.select(0)
    enable_button(button_unit_remove)
    if len(units) > 19:
        disable_button(button_unit_add)


func _on_button_unit_remove_pressed():
    var item = tree_units.get_selected()
    var new_item_to_select = null
    if item.get_next():
        new_item_to_select = item.get_next()
    elif item.get_prev():
        new_item_to_select = item.get_prev()
    
    unit_names.erase(current_unit.name)
    units.erase(current_unit)
    tree_units.get_root().remove_child(item)
    tree_units.set_selected(new_item_to_select, 0)
    
    if units == []:
        disable_button(button_unit_remove)
        disable_button(button_task_left)
        disable_button(button_task_right)
        vbox_task.hide()
    enable_button(button_unit_add)


func _on_tree_units_item_edited():
    var new_name = tree_units.get_selected().get_text(0)
    var index = unit_names.find(current_unit.name)
    current_unit.name = new_name
    label_unit.text = new_name
    unit_names[index] = new_name


func _on_tree_units_item_selected():
    var index = tree_units.get_selected().get_index()
    current_unit = units[index]
    current_task = null
    change_current_task(0)


func _on_button_category_add_pressed():
    if not tree_categories.get_root():
        tree_categories.create_item()
    var category_name = get_new_name(tr("New Category"), categories)
    var category_item = tree_categories.create_item(tree_categories.get_root())
    categories.append(category_name)
    category_item.set_editable(0, true)
    category_item.set_text(0, category_name)
    
    category_item.select(0)
    enable_button(button_category_remove)
    if len(categories) > 5:
        disable_button(button_category_add)

    button_category.add_item(category_name)
    if current_task and len(categories) == 1:
        button_category.select(-1)
    check_task_integrity()


func _on_button_category_remove_pressed():
    var item = tree_categories.get_selected()
    if not item:
        return
    
    categories.erase(item.get_text(0))
    button_category.remove_item(item.get_index())
    tree_categories.get_root().remove_child(item)
    tree_categories.deselect_all()
    
    check_task_integrity()
    if categories == []:
        disable_button(button_category_remove)
    enable_button(button_category_add)


func _on_tree_categories_item_edited():
    var index = tree_categories.get_selected().get_index()
    var new_name = tree_categories.get_selected().get_text(0)
    categories[index] = new_name
    button_category.set_item_text(index, new_name)
    if OS.get_name() == "Android":
        tree_categories.deselect_all()
        vbox_editor.position.y = 0
        set_process(false)


func _on_tree_categories_item_selected():
    if OS.get_name() == "Android":
        set_process(true)


func _on_button_task_left_pressed():
    change_current_task(-1)


func _on_button_task_right_pressed():
    change_current_task(1)


func _on_button_category_item_selected(index):
    current_task.category = button_category.get_item_text(index)
    check_task_integrity()


func _on_button_difficulty_toggled(_button_pressed, button_number):
    current_task.difficulty = button_number
    check_task_integrity()


func _on_line_question_text_changed():
    current_task.question = line_question.text
    check_task_integrity()


func _on_line_answer_text_changed(_new_text):
    current_task.answer = line_answer.text
    check_task_integrity()


func _on_line_focus_entered():
    if OS.get_name() == "Android":
        set_process(true)


func _on_line_focus_exited():
    if OS.get_name() == "Android":
        vbox_editor.position.y = 0
        set_process(false)


func _on_button_task_remove_pressed():
    var index = current_unit.tasks.find(current_task)
    current_unit.tasks.erase(current_task)
    if current_unit.tasks == []:
        create_new_task()
    else:
        if index < len(current_unit.tasks):
            current_task = current_unit.tasks[index]
        else:
            current_task = current_unit.tasks[index - 1]
    show_current_task()
    check_task_integrity()


func _on_button_dialog_back_pressed():
    message_dialog.hide()


func _on_button_dialog_close_pressed():
    message_dialog.hide()
    if warning_type == "new_file":
        clear_all_data()
    if warning_type == "open_file":
        popup_open_file_dialog()
    if warning_type == "close":
        scene_transition.change_scene("res://scenes/main.tscn")


func _on_native_file_dialog_open_file(_status, selected_paths, _selected_filter_index):
    if selected_paths.is_empty():
        return
    var path = selected_paths[0]
    var file = FileAccess.open(path, FileAccess.READ)
    load_json_data([path, file.get_as_text()])


func _on_native_file_dialog_save_file(_status, selected_paths, _selected_filter_index):
    if selected_paths.is_empty():
        return
    var path = selected_paths[0]
    path = path + ".json" if not path.to_lower().ends_with(".json") else path
    get_tasks_to_save()
    save_json_data(path)


func _on_file_dialog_file_selected(path):
    path = path[0] if typeof(path) == TYPE_PACKED_STRING_ARRAY else path
    if file_dialog.file_mode == 0:
        var file = FileAccess.open(path, FileAccess.READ)
        load_json_data([path, file.get_as_text()])
    if file_dialog.file_mode >= 3:
        path = path + ".json" if not path.to_lower().ends_with(".json") else path
        get_tasks_to_save()
        save_json_data(path)


func build_file_dialog():
    if OS.get_name() in ["Linux", "macOS", "Windows"]:
        return
    elif OS.get_name() == "Web":
        java_script_callback = JavaScriptBridge.create_callback(load_json_data)
        JavaScriptBridge.get_interface("web_file_dialog").dataLoaded = java_script_callback
        label_file.hide()
        return
    file_dialog = load("res://scenes/file_dialog.tscn").instantiate()
    file_dialog.access = 2
    file_dialog.set_filters(json_filter)
    file_dialog.file_selected.connect(_on_file_dialog_file_selected)
    file_dialog.files_selected.connect(_on_file_dialog_file_selected)
    add_child(file_dialog)


func popup_open_file_dialog():
    if OS.get_name() in ["Linux", "macOS", "Windows"]:
        var title = tr("Open a File")
        var mode = DisplayServer.FILE_DIALOG_MODE_OPEN_FILE
        DisplayServer.file_dialog_show(title, global.default_dir, "", false, mode, json_filter,
            _on_native_file_dialog_open_file)
    elif OS.get_name() == "Web":
        JavaScriptBridge.eval("openFile()")
    else:
        file_dialog.file_mode = 0
        file_dialog.cancel_button_text = tr("Cancel")
        file_dialog.ok_button_text = tr("Open")
        file_dialog.current_dir = global.default_dir
        file_dialog.popup_centered(Vector2i(1600, 900))


func load_json_data(data):
    var path = data[0]
    var string = data[1]
    var json = JSON.new()
    var error = json.parse(string)
    
    if error == OK:
        tasks_on_load = json.data
        unit_names = tasks_on_load.keys()
        get_units()
        get_tasks_and_categories()
        current_file_path = path
        label_file.text = path.get_file()
        tree_units.set_selected(tree_units.get_root().get_child(0), 0)
    else:
        show_error_dialog(tr("ERROR:\nThe file could not be opened!"))


func save_json_data(path):
    var data = JSON.stringify(tasks_to_save, "  ")
    if OS.get_name() == "Web":
        var buffer = data.to_utf8_buffer()
        JavaScriptBridge.download_buffer(buffer, path, "application/json")
    else:
        var file = FileAccess.open(path, FileAccess.WRITE)
        file.store_string(data)
    tasks_on_load = tasks_to_save.duplicate(true)
    current_file_path = path
    label_file.text = path.get_file()
    animation_save.play("save_file")


func get_units():
    tree_units.clear()
    units = []
    
    var root = tree_units.create_item()
    for i in range(len(unit_names)):
        var unit_item = tree_units.create_item(root)
        unit_item.set_editable(0, true)
        unit_item.set_text(0, unit_names[i])
        var unit_object = global.unit.new(i, unit_item, unit_names[i])
        units.append(unit_object)
    
    enable_button(button_unit_remove)
    if len(units) > 19:
        disable_button(button_unit_add)


func get_tasks_and_categories():
    tree_categories.clear()
    button_category.clear()
    categories = []

    for unit in units:
        for task in tasks_on_load[unit.name]:
            if not "id" in task or not "category" in task or not "difficulty" in task \
                    or not "question" in task or not "answer" in task:
                continue
            var task_object = global.task.new(task.id, unit.name, task.category, task.difficulty,
                    task.question, task.answer)
            unit.tasks.append(task_object)
            if not task.category in categories:
                categories.append(task.category)
    
    if categories == []:
        return
    categories.sort()
    enable_button(button_category_remove)
    if len(categories) > 5:
        disable_button(button_category_add)

    var root = tree_categories.create_item()
    for category in categories:
        var item = tree_categories.create_item(root)
        item.set_editable(0, true)
        item.set_text(0, category)
        button_category.add_item(category)


func clear_all_data():
    categories = []
    current_file_path = ""
    current_task = null
    current_unit = null
    unit_names = []
    units = []

    tree_units.clear()
    tree_categories.clear()
    button_category.clear()
    label_file.text = tr("New File")
    line_question.text = ""
    line_answer.text = ""
    disable_button(button_unit_remove)
    disable_button(button_category_remove)
    disable_button(button_task_left)
    disable_button(button_task_right)
    vbox_task.hide()


func get_tasks_to_save():
    tasks_to_save = {}
    # sort units by name in natural order
    units.sort_custom(func(a, b): return a.name.naturalnocasecmp_to(b.name) < 0)
    for unit in units:
        tasks_to_save[unit.name] = []
        for task in unit.tasks:
            if task.category and task.difficulty and task.question and task.answer:
                var task_dict = {"answer": task.answer, "category": task.category, "difficulty":
                        float(task.difficulty), "id": float(task.id), "question": task.question}
                tasks_to_save[unit.name].append(task_dict)


func get_new_id(objects):
    var ids = []
    for object in objects:
        ids.append(int(object.id))
    var check_id = true
    var new_id = -1
    while check_id:
        new_id += 1
        if not new_id in ids:
            check_id = false
            return new_id


func get_new_name(base_name, names):
    var new_name = base_name
    if not new_name in names:
        return new_name
    
    var increase_number = true
    var number = 1
    while increase_number:
        number += 1
        new_name = "%s %s" % [base_name, str(number)]
        if not new_name in names:
            increase_number = false
            return new_name


func change_current_task(value):
    if current_task == null:
        if current_unit.tasks:
            current_task = current_unit.tasks[-1]
        else:
            create_new_task()
    else:
        var new_index = current_unit.tasks.find(current_task) + value
        if new_index < len(current_unit.tasks):
            current_task = current_unit.tasks[new_index]
        else:
            create_new_task()
    
    show_current_task()
    check_task_integrity()
    vbox_task.show()


func create_new_task():
    var task_id = get_new_id(current_unit.tasks)
    var task_object = global.task.new(task_id, current_unit.name, null, 2, "", "")
    current_unit.tasks.append(task_object)
    current_task = task_object


func show_current_task():
    label_unit.text = current_unit.name
    button_category.select(categories.find(current_task.category))
    if current_task.difficulty == 1:
        button_easy.set_pressed(true)
    if current_task.difficulty == 2:
        button_medium.set_pressed(true)
    if current_task.difficulty == 3:
        button_hard.set_pressed(true)
    line_question.text = current_task.question
    line_answer.text = current_task.answer

    var task_text = "%s / %s"
    var task_number = current_unit.tasks.find(current_task) + 1
    label_task_number.text = task_text % [task_number, len(current_unit.tasks)]

    var task_index = current_unit.tasks.find(current_task)
    if task_index == 0:
        disable_button(button_task_left)
    else:
        enable_button(button_task_left)
    if task_index == len(current_unit.tasks) - 1:
        button_task_right.get_child(0).texture = load("res://assets/ui/icon_list_add.svg")
    else:
        button_task_right.get_child(0).texture = load("res://assets/ui/icon_go_next.svg")


func check_task_integrity():
    if button_category.get_selected_id() == -1 or line_question.text == "" or line_answer.text == "":
        hbox_warning.show()
        disable_button(button_task_right)
    else:
        hbox_warning.hide()
        enable_button(button_task_right)


func disable_button(button):
    button.disabled = true
    button.get_child(0).modulate = Color(1, 1, 1, 0.5)


func enable_button(button):
    button.disabled = false
    button.get_child(0).modulate = Color(1, 1, 1, 1)


func show_error_dialog(error_text):
    if file_dialog:
        file_dialog.hide()
    label_dialog.text = error_text
    button_dialog_close.hide()
    message_dialog.show()


func show_warning_dialog(warning_text, button_text):
    label_dialog.text = warning_text
    button_dialog_close.get_child(0).text = button_text
    button_dialog_close.show()
    message_dialog.show()
