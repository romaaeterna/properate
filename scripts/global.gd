class_name Global
extends Node


class event:
    var id
    var text
    var modificator

    func _init(event_id, event_text, event_modificator):
        self.id = event_id
        self.text = event_text
        self.modificator = event_modificator


class task:
    var id
    var unit
    var category
    var difficulty
    var question
    var answer

    func _init(task_id, task_unit, task_category, task_difficulty, task_question, task_answer):
        self.id = task_id
        self.unit = task_unit
        self.category = task_category
        self.difficulty = task_difficulty
        self.question = task_question
        self.answer = task_answer


class team:
    var number
    var name
    var sprite
    var frames
    var meters
    var sound

    func _init(team_number):
        self.number = team_number
        self.name = ""
        self.sprite = null
        self.frames = null
        self.meters = 0
        self.sound = null


class unit:
    var id
    var item
    var name
    var tasks

    func _init(unit_id, unit_item, unit_name):
        self.id = unit_id
        self.item = unit_item
        self.name = unit_name
        self.tasks = []


# internal variables
var documents_dir = OS.get_system_dir(OS.SYSTEM_DIR_DOCUMENTS)
var game_start = true
var last_file = ""
var last_scene = "main"

# network
var host = false
var client_index = 0
var clients = []
var lobby = ""
var online_timer = 2
var peer = null

# general settings
var cheat_mode = false
var countdown_length = 3
var default_dir = documents_dir
var events = 0.1
var language = OS.get_locale_language()
var music = 1.0
var sounds = 1.0

## game setup
# background
var background = "random"
var background_desert = load("res://assets/textures/background_desert.png")
var background_forest = load("res://assets/textures/background_forest.png")
var background_grassland = load("res://assets/textures/background_grassland.png")
var background_random = load("res://assets/textures/background_random.png")
# tasks
var tasks = []
var tasks_easy = []
var tasks_medium = []
var tasks_hard = []
# teams
var team1 = team.new(1)
var team2 = team.new(2)
var team3 = team.new(3)
var team4 = team.new(4)
var team_number = 2
# various
var events_start = []
var events_meantime = []
var events_end = []
var game_distance = 2
var game_mode = "team"
var player_number = 10


func _init():
    randomize()
    load_settings()
    load_events()


func load_settings():
    var config = ConfigFile.new()
    config.load("user://settings.ini")

    cheat_mode = config.get_value("General", "cheat_mode", false)
    countdown_length = config.get_value("General", "countdown", 3)
    default_dir = config.get_value("General", "default_dir", documents_dir)
    events = config.get_value("General", "events", 0.1)
    language = config.get_value("General", "language", OS.get_locale_language())
    music = config.get_value("General", "music", 1.0)
    sounds = config.get_value("General", "sounds", 1.0)
    
    TranslationServer.set_locale(language)
    AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Music"), linear_to_db(music))
    AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Sounds"), linear_to_db(sounds))


func load_events():
    events_start = []
    events_meantime = []
    events_end = []
    
    var path
    if Time.get_date_dict_from_system()["month"] != 12:
        path = "res://data/events_%s.json" % language
        if not ResourceLoader.exists(path):
            path = "res://data/events_en.json"
    else:
        path = "res://data/events_xmas_%s.json" % language
        if not ResourceLoader.exists(path):
            path = "res://data/events_xmas_en.json"
    var file = FileAccess.open(path, FileAccess.READ)
    var json = JSON.new()
    json.parse(file.get_as_text())
    
    for element in json.data["start"]:
        var event_object = event.new(element.id, element.text, element.modificator)
        events_start.append(event_object)
    for element in json.data["meantime"]:
        var event_object = event.new(element.id, element.text, element.modificator)
        events_meantime.append(event_object)
    for element in json.data["end"]:
        var event_object = event.new(element.id, element.text, element.modificator)
        events_end.append(event_object)
