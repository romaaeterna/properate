extends CanvasLayer


func change_scene(new_scene_file):
    $Animation.play("dissolve")
    await $Animation.animation_finished
    get_tree().change_scene_to_file(new_scene_file)
    $Animation.play_backwards("dissolve")


func fade_out():
    $Animation.play("fade_out")
    await $Animation.animation_finished
    get_tree().quit()
    if OS.get_name() == "Web":
        JavaScriptBridge.eval("window.history.back()")
