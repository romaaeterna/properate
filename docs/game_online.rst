Online Game
===========


Setting up the game
-------------------

After starting the game you'll see the main window of the application.
Click the button "Play Game" to open the setup screen.

.. image:: images/screen_main.png

Type in the name of the lobby you want to either create or join. The online game mode can be played by 2 to 4 players.

.. image:: images/screen_setup.png

Once you've created the lobby, you'll need to set up the game as a host:

1. Click the button at the top left and navigate to the file you want to open. Then choose the units you want to repeat.
2. Set the game distance, timer duration and background image. 
3. Enter your name, select your character and click the "Ready" button.
4. Click the button "Start Game" when all players are ready.

.. image:: images/screen_setup_online.png

If you've joined the lobby, all you need to do is enter your name, select your character and click the "Ready" button.


Playing the game
----------------

The objective of the game is to be the first player to reach the finish line on the right edge of the screen.

.. image:: images/screen_game_online_1.png

The online game is turn-based. The current player has to select the level of difficulty determing the running distance.

.. image:: images/screen_game_online_2.png

Now a randomly generated question will be shown. The current player has to type in the answer.

.. image:: images/screen_game_online_3.png

Then the other players have to decide if the given answer was correct. The majority decides.

.. image:: images/screen_game_online_4.png

The current player gets a feedback und will move if the given answer was correct.

.. image:: images/screen_game_online_5.png

May the best win!

.. image:: images/screen_game_online_6.png
