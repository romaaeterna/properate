Properate!
==========

.. image:: images/logo.png
    :align: center

****

**Properate! (Latin for "Hurry up!") is a game made for students to learn while having fun.
It can be played in teams or small groups – locally or online.**

The program is released under the `GNU General Public License (GPL) version 3 <https://www.gnu.org/licenses/gpl-3.0>`_.


Contents
--------

.. toctree::
   :maxdepth: 2

   installation
   getting_started
   game_local
   game_online
   development
   credits
