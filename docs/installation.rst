Installation
============


Microsoft Windows
-----------------

To install Properate! follow the setup wizard of `ProperateSetup.exe <https://romaaeterna.itch.io/properate>`_.

If you don't want to install the game, just unzip `ProperateWindows.zip <https://romaaeterna.itch.io/properate>`_. and execute the file ``Properate.exe``.


Linux
-----

To play Properate! just unzip `ProperateLinux.zip <https://romaaeterna.itch.io/properate>`_. and execute the file ``Properate.sh``.


Build with Godot Engine
-----------------------

If you want to build the game on your own, modify or improve it, feel free to do so.
All you need is the `Godot Engine <https://godotengine.org/>`_ (>= 4.1) and the `source code hosted on GitLab <https://gitlab.com/romaaeterna/properate>`_. 
