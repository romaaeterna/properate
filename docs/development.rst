Development
===========

The current version of Properate! is 1.0.0.


Bugs
----

Known Bugs
~~~~~~~~~~

At the moment no bugs are known.

Filing A Bug
~~~~~~~~~~~~

If you’ve found a bug in Properate!, please head over to GitLab and `file a report <https://gitlab.com/romaaeterna/properate/issues>`_.
Filing bugs helps improve the software for everyone.


Source Code
-----------

The full source code of Properate! is hosted on `GitLab <https://gitlab.com/romaaeterna/properate>`_.
