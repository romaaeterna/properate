Credits
=======


Properate! is developed by Thomas Dähnrich and Lara-Sophie Nitzsche.

Special thanks go to:

- the developers of the `Godot Engine <https://godotengine.org/>`_
- the Latin students of the `Vicco-von-Bülow-Gymnasium Falkensee <http://www.vicco-von-buelow-gymnasium-falkensee.de/>`_ (for being enthusiastic beta testers)
