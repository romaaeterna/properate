Getting started
===============


Tasks
-----

To play Properate! you need to load a file with tasks. 
Just use the build-in editor, which features easy and intuitive operation, to create new tasks or edit existing ones.

.. image:: images/screen_editor_1.png

You need at least one unit and category (e.g. grammar, vocabulary) and one task per level of difficulty.

.. image:: images/screen_editor_2.png

Please ensure that incomplete tasks are not saved.


Teams and player numbers
------------------------

Properate! has got a game mode for playing in up to four competitive teams.
Both teams should be equally big and consist of 4 to 16 players.
Each team player has to be allocated a number.
Players with the same number compete against each other.
When every student is assigned to his/her team and number, you can start the game.


Settings
--------

There are several general settings you may adapt to your needs.

.. image:: images/screen_settings.png
