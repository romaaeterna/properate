Local Game
==========


Setting up the game
-------------------

After starting the game you'll see the main window of the application.
Click the button "Play Game" to open the setup screen.

.. image:: images/screen_main.png

Select one of the following two local game modes:

- Team mode: for 2 to 4 teams, perfect for playing in class
- Player mode: for 2 to 4 players, perfect for playing in small groups

.. image:: images/screen_setup.png

Now you can setup the game to your liking:

1. Click the button at the top left and navigate to the file you want to open. Then choose the units you want your students to repeat.
2. Set the game distance, number of players per team (only in "team mode") and background image. 
3. Enter the names of the players and select the characters.
4. Finally click the button "Start Game".

.. image:: images/screen_setup_local.png


Playing the game
----------------

The objective of the game is to be the first player / team to reach the finish line on the right edge of the screen.

.. image:: images/screen_game_local_1.png

"Player mode": The current player will be chosen turn-based. He/She has to select the level of difficulty determing the running distance.

.. image:: images/screen_game_local_2.png

"Team mode": The current player's number will be chosen randomly. One player has to select the level of difficulty determing the running distance.

.. image:: images/screen_game_local_3.png

Now a randomly generated question will be shown (in "team mode" after a countdown).

.. image:: images/screen_game_local_4.png

"Player mode": Just select if the given answer was right or wrong. 

.. image:: images/screen_game_local_5.png

"Team mode": The player giving the fastest correct answer will be the one moving forward.

.. image:: images/screen_game_local_6.png

May the best win!

.. image:: images/screen_game_local_7.png

