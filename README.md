# Properate!

## Introduction

Properate! (Latin for "Hurry up!") is a game made for students to learn while having fun.
It can be played in teams or small groups – locally or online.

You can play the game online or download it from the [Itch.io website](https://romaaeterna.itch.io/properate).

See the [online documentation](https://properate.readthedocs.io/en/latest/) for instructions on how to play.

## Source code & License

The source code of Properate! is hosted on [GitLab](https://gitlab.com/romaaeterna/properate).

The game is released under the [GNU General Public License (GPL) version 3](https://www.gnu.org/licenses/gpl-3.0).
See the file [LICENSE.txt](LICENSE.txt) for more information.

## Special thanks to

- the developers of the [Godot Engine](https://godotengine.org/)
- the Latin students of the [Vicco-von-Bülow-Gymnasium Falkensee](http://www.vicco-von-buelow-gymnasium-falkensee.de/) (for being enthusiastic beta testers)
