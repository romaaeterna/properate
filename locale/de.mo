��    �      �  �         �  3   �       /        B     H     O     U  	   W     a     h     p     y     ~  -   �  +   �     �     �  
     	     
        %  %   2  %   X     ~     �     �     �     �     �     �     �            /   "     R  
   d     o  $   {  #   �     �     �     �     �  
   �     �     �  
     
               
   1  
   <     G  	   P     Z     c  
   o     z  	   �     �     �     �     �     �     �     �     �  
   �     �     �               %     .     3     ?  	   D     N     Z  	   n  z   x  �   �  	   �  C   �     �     �  	   �     �  	          	           	   )     3     ?  $   P     u      �  7   �  6   �      $  &   E  $   l  /   �  -   �  8   �  6   (  3   _     �     �  	   �     �     �     �     �     �     �     �  	                       2     @     M     V     b     o     v  
   ~     �     �     �     �     �     �  	   �     �     �  "   �  .     !   5  ,   W  (   �     �  ,   �  &   �          $     1  6   :     q     �     �     �     �  ?   �  D   �  B   /  '   r  "   �     �      �     �  !        3     Q     ^     f     s     x          �     �     �    �  5   �     �  6   �     /     5     <     B     D     R     Z     c     j     r  *     '   �  	   �     �  
   �  
               )   +  )   U       
   �     �     �     �     �                 8   	   @   B   J      �      �      �   0   �   &   �      !!     (!     /!     8!     T!     a!     n!     {!     �!     �!     �!     �!     �!     �!     �!     �!     �!     �!  #   "  
   ("     3"     :"     @"     F"  
   U"     `"     l"     y"     �"     �"     �"     �"     �"     �"     �"     �"     �"     �"     #     #     4#  �   <#  �   �#  
   �$  M   �$     �$  	   �$  
   %  	   %  
   %  	   !%  
   +%  	   6%  
   @%     K%     Y%  )   i%  %   �%  )   �%  C   �%  C   '&  #   k&  -   �&  *   �&  4   �&  1   '  E   O'  E   �'  9   �'     (     -(     3(     :(     B(     I(     U(  '   c(     �(  	   �(     �(     �(  
   �(     �(     �(     �(     )     )     !)     -)     :)     B)     P)     a)     i)     p)     w)     ~)  
   �)     �)     �)  *   �)  ?   �)  *   *  7   D*  .   |*     �*  *   �*  .   �*     +     /+     @+  6   M+     �+     �+  	   �+     �+     �+  N   �+  R   ,  S   j,  /   �,  ,   �,     -  !   3-     U-      r-     �-  	   �-     �-     �-     �-     �-     �-     �-     �-     �-               �   L      "   Y      .      H           �   �           Z   �      #   M          �   O           b         B   2      �           z   
           !       �   �   \   r   c   P   1       �       f   l       �       /   I   �      4   �   a       j       |   �       R   �   �   	   s           y   i   m   �      ;   t   �   �      x      �   �   E           <   }       K   �       @   =       �   J      6             o   8       0   �   [   �   A   ?   Q   �      g       �       *          n   h   {           �   U   �   9   �      5   -   k   S   T   F   �   '   )   �      �       $   D   3           7   %   w   �                             �           (   �   ~   X           N   +   `   �   �       �           �   �   :   �       �       &          e       p   �   �   _   �   V       q   d       >      �         ,              W       C       �          G       ]   �   ^   u       �       v        %s
is choosing the difficulty of the next question. %s s %s:
Choose the difficulty of the next question! 100 m 1000 m 400 m ? A Game by Answer Answer: Awesome! Back Background: Be the first player to reach the finish line! Be the first team to reach the finish line! Cancel Cannot connect to server! Categories Category: Cheat Mode Check Answer Check the answer of player %s … – Click "Ready" when your are finished! Click "Start Game"! Close Close without Saving Code Snippets & Addons Congratulations! Connecting to Server … Create Lobby Create without Saving Credits Damn it! Data is incomplete! The task will not be saved! Default Directory Difficulty Difficulty: ERROR:
The file could not be opened! ERROR:
The file data is incomplete! Easy Editor Event Event Chance Fantastic! Fonts Game Distance: Game Setup Game Start Hard How disappointing! How great! Join Lobby Language Let's go! Licenses Lobby Name: Local Game Made with Godot Engine 4 Main Menu Medium Music Name: New Category New File New Game New Unit Next Player Next Round Next Round – No File Loaded None Number of Players: Oh dear! Okay Online Game Open Open File Open a File Open without Saving Play Game Play the game in class:
• either in opposing teams of up to 16 players,
• or in an one-on-one match of 2 to 4 players. Play the game online with 2 to 4 players:
• one player is the host who creates the lobby and sets up the game,
• the other players join this lobby. Player %s Player %s of Team "%s":
Choose the difficulty of the next question! Player %s: %s Player 1 Player 1: Player 2 Player 2: Player 3 Player 3: Player 4 Player 4: Player Mode Player selection Please choose a different character! Please open a file with tasks! Please select at least one unit! Please select units including all levels of difficulty! Please select units inluding all levels of difficulty! Please type in a different name! Please type in a name for each player! Please type in a name for each team! Please type in different names for each player! Please type in different names for each team! Please type in the name of the lobby you want to create! Please type in the name of the lobby you want to join! Please type in your name and choose your character! Programming & Design Question Question: Quit Ready Rematch! Restore Restore default settings Right Answer Save Save File Save a File Select Select a Directory Select a File Sensational! Settings Show Answer Solution: %s Sounds Sprites Start Game Submit Answer Task Team 1 Team 2 Team 3 Team 4 Team Mode Textures & Icons That's amazing! The game was canceled by the host. The game you want to join has already started. The lobby was closed by the host. The lobby you want to create already exists. The maximum number of lobbys is reached. The winner is:
%s! There are already four players in the lobby. There is no lobby with the given name. Timer Duration: Timer Length Too bad! Turn on/off context click for relocation of characters Type in your answer! – Unit: Units Version: %s Victory WARNING:
If you close the window all changes will be discarded! WARNING:
If you create a new document all changes will be discarded! WARNING:
If you open a new document all changes will be discarded! Waiting for all players to be ready … Waiting for game start by host … Waiting for host … Waiting for other player … – Waiting for other players … Waiting for other players … – Waiting for player %s … – What a pity! Writing Wrong Answer long normal short © 2023/2024 Roma Aeterna   Project-Id-Version: Properate!
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.4.2
X-Poedit-Basepath: .
 %s
wählt den Schwierigkeitsgrad der nächsten Frage. %s s %s:
Wähle den Schwierigkeitsgrad der nächsten Frage! 100 m 1000 m 400 m ? Ein Spiel von Antwort Antwort: Super! Zurück Hintergrund: Erreiche als erster Spieler die Ziellinie! Erreiche als erstes Team die Ziellinie! Abbrechen Keine Verbindung zum Server! Kategorien Kategorie: Cheat-Modus Antwort prüfen Prüfe die Antwort von Spieler %s … – Klicke auf "Bereit", wenn du fertig bist! Klicke auf "Spiel starten"! Schließen Schließen ohne zu speichern Code-Schnipsel & Addons Herzlichen Glückwunsch! Verbinde mit Server … Lobby erstellen Erstellen ohne zu speichern Credits Verdammt! Die Daten sind unvollständig! Die Aufgabe wird nicht gespeichert! Standard-Verzeichnis Schwierigkeitsgrad Schwierigkeitsgrad: FEHLER:
Die Datei konnte nicht geöffnet werden! FEHLER:
Die Daten sind unvollständig! Leicht Editor Ereignis Ereignis-Wahrscheinlichkeit Fantastisch! Schriftarten Spiellänge: Spiel-Setup Spielbeginn Schwer Wie enttäuschend! Großartig! Lobby beitreten Sprache Los geht's! Lizenzen Name der Lobby: Lokales Spiel Programmiert mit der Godot Engine 4 Hauptmenü Mittel Musik Name: Neue Kategorie Neue Datei Neues Spiel Neue Lektion Nächster Spieler Nächste Runde Nächste Runde – Keine Datei geladen Keines Anzahl der Spieler: Oh, je! Okay Online-Spiel Öffnen Datei öffnen Datei öffnen Öffnen ohne zu speichern Spielen Das Spiel in der Klasse spielen:
• entweder in gegnerischen Teams von bis zu 16 Spielern,
• oder in einem Spiel jeder gegen jeden mit 2 bis 4 Spielern. Das Spiel online mit 2 bis 4 Spielern spielen:
• ein Spieler erstellt als Gastgeber die Lobby und nimmt die Spieleinstellungen vor,
• die anderen Spieler treten dieser Lobby bei. Spieler %s Spieler %s des Teams "%s":
Wähle den Schwierigkeitsgrad der nächsten Frage! Spieler %s: %s Spieler 1 Spieler 1: Spieler 2 Spieler 2: Spieler 3 Spieler 3: Spieler 4 Spieler 4: Spieler-Modus Spieler-Auswahl Bitte einen anderen Charakter auswählen! Bitte öffne eine Datei mit Aufgaben! Bitte wähle mindestens eine Lektion aus! Bitte wähle Lektionen aus, die alle Schwierigkeitsgrade enthalten! Bitte wähle Lektionen aus, die alle Schwierigkeitsgrade enthalten! Bitte einen anderen Namen eingeben! Bitte gib einen Namen für jeden Spieler ein! Bitte gib einen Namen für jedes Team ein! Bitte gib verschiedene Namen für jeden Spieler ein! Bitte gib verschiedene Namen für jedes Team ein! Bitte gib einen Namen für die Lobby ein, die du erstellen möchtest! Bitte gib einen Namen für die Lobby ein, der du beitreten möchtest! Bitte gib einen Namen ein und wähle einen Charakter aus! Programmierung & Design Frage Frage: Beenden Bereit Rückspiel! Zurücksetzen Standard-Einstellungen wiederherstellen Richtige Antwort Speichern Datei speichern Datei speichern Auswählen Ordner auswählen Datei auswählen Sensationell! Einstellungen Antwort anzeigen Lösung: %s Soundeffekte Sprites Spiel starten Antwort absenden Aufgabe Team 1 Team 2 Team 3 Team 4 Team-Modus Texturen & Icons Echt knorke! Das Spiel wurde vom Gastgeber abgebrochen. Das Spiel, dem du beitreten möchtest, wurde bereits gestartet. Die Lobby wurde vom Gastgeber geschlossen. Die Lobby, die du erstellen möchtest, gibt es bereits. Die maximale Anzahl von Lobbys wurde erreicht. Der Sieger heißt:
%s! Es gibt bereits vier Spieler in der Lobby. Es gibt keine Lobby mit dem angegebenen Namen. Länge des Timers: Dauer des Timers So ein Pech! Verschieben der Figur mit Kontextklick an-/ausschalten Bitte gib eine Antwort ein! – Lektion: Lektionen Version: %s Sieg WARNUNG:
Wenn das Fenster geschlossen wird, werden alle Änderungen verworfen! WARNUNG:
Wenn ein neues Dokument erstellt wird, werden alle Änderungen verworfen! WARNUNG:
Wenn ein neues Dokument geöffnet wird, werden alle Änderungen verworfen! Warte darauf, dass alle Spieler bereit sind … Warte auf Spielstart durch den Gastgeber … Warte auf Gastgeber … Warte auf anderen Spieler … – Warte auf andere Spieler … Warte auf andere Spieler … – Warte auf Spieler %s … – Verflixt! Texte Falsche Antwort lang normal kurz © 2023/2024 Roma Aeterna   